#
# shell script to download kernel source and patch it
#

# Download kernel
git clone git://kernel.ubuntu.com/ubuntu/ubuntu-xenial.git
cd ./ubuntu-xenial
git checkout Ubuntu-4.4.0-96.119

# Patch it
git am ../msm_patches/0001-cfg80211-Use-new-wiphy-flag-WIPHY_FLAG_DFS_OFFLOAD.patch
git am ../msm_patches/0002-cfg80211-Add-new-wiphy-flag-WIPHY_FLAG_DFS_OFFLOAD.patch
git am ../msm_patches/0003-cfg80211-avoid-restoring-the-regulatory-during-disco.patch
git am ../msm_patches/0004-cfg80211-unblock-user-hint-when-cfg80211_regdom-is-i.patch
git am ../msm_patches/0005-cfg80211-Bypass-checkin-the-CHAN_RADAR-if-DFS_OFFLOA.patch
git am ../msm_patches/0006-cfg80211-make-wdev_list-accessible-to-drivers.patch
git am ../msm_patches/0007-cfg80211-Do-not-disconnect-on-suspend.patch
git am ../msm_patches/0008-net-lro-extend-LRO-to-use-hardware-assists.patch
git am ../msm_patches/0009-cfg80211-export-regulatory_hint_user-API.patch
git am ../msm_patches/0010-mac80211-implement-HS2.0-gratuitous-ARP-unsolicited-.patch
git am ../msm_patches/0011-add-cnss2.h-from-cnss2-driver.patch
git am ../msm_patches/0012-x86-kernel-reserve-CMA-memory-space-under-4G.patch
git am ../msm_patches/0013-cfg80211-indicate-support-for-external-authenticatio.patch
git am ../msm_patches/0014-nl80211-Allow-SAE-Authentication-for-NL80211_CMD_CON.patch
git am ../msm_patches/0015-cfg80211-NL80211_ATTR_SOCKET_OWNER-support-for-CMD_C.patch
git am ../msm_patches/0016-cfg80211-nl80211-Optional-authentication-offload-to-.patch
git am ../msm_patches/0017-nl80211-Fix-external_auth-check-for-offloaded-authen.patch
git am ../msm_patches/0018-cfg80211-Sync-nl80211_commands-attrs-with-upstream-s.patch
git am ../msm_patches/0034-Remove-from-kernel-version.patch
git am ../msm_patches/0040-cfg80211-allow-finding-vendor-with-OUI-without-speci.patch
git am ../msm_patches/0041-cfg80211-Define-IEEE-P802.11ai-FILS-information-elem.patch
git am ../msm_patches/0042-cfg80211-add-helper-to-find-an-IE-that-matches-a-byt.patch
git am ../msm_patches/0043-dsa-mv88e6xxx-Optimise-atu_get.patch
git am ../msm_patches/0044-cfg80211-add-and-use-strongly-typed-element-iteratio.patch
git am ../msm_patches/0045-cfg80211-Parsing-of-Multiple-BSSID-information-in-sc.patch
git am ../msm_patches/0046-cfg80211-use-for_each_element-for-multi-bssid-parsin.patch
git am ../msm_patches/0047-cfg80211-Properly-track-transmitting-and-non-transmi.patch
git am ../msm_patches/0048-cfg80211-Move-Multiple-BSS-info-to-struct-cfg80211_b.patch
git am ../msm_patches/0049-cfg80211-parse-multi-bssid-only-if-HW-supports-it.patch
git am ../msm_patches/0050-cfg80211-make-BSSID-generation-function-inline.patch
git am ../msm_patches/0051-cfg80211-add-various-struct-element-finding-helpers.patch
git am ../msm_patches/0052-cfg80211-save-multi-bssid-properties.patch
git am ../msm_patches/0053-cfg80211-fix-the-IE-inheritance-of-extension-IEs.patch
git am ../msm_patches/0054-cfg80211-fix-memory-leak-of-new_ie.patch
git am ../msm_patches/0055-cfg80211-add-missing-kernel-doc-for-multi-BSSID-fiel.patch
git am ../msm_patches/0056-ieee80211-fix-for_each_element_extid.patch
git am ../msm_patches/0057-cfg80211-fix-and-clean-up-cfg80211_gen_new_bssid.patch
git am ../msm_patches/0058-cfg80211-Add-option-to-specify-previous-BSSID-for-Co.patch
git am ../msm_patches/0059-cfg80211-Define-macro-to-indicate-prev_bssid-connect.patch
#update config
sudo cp ../msm_patches/config-4.4.83+ .config
sudo make-kpkg -j4 --initrd kernel_image kernel_headers

