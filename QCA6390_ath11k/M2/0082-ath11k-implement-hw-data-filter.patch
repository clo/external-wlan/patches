From df907e1e0ba2442725eba18452446fff0b4d24ea Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Mon, 16 Mar 2020 10:06:29 +0800
Subject: [PATCH 082/124] ath11k: implement hw data filter

Host needs to set hw data filter before entering WoW to
avoid frequent wakeup. Host clears hw data filter when
leaving WoW.

Tested-on: QCA6390 WLAN.HST.1.0.1-01230-QCAHSTSWPLZ_V2_TO_X86-1

Change-Id: I76dc9fbd508d333c4ecb1c5e63d572432a271467
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/wmi.c | 32 +++++++++++++++++++++
 drivers/net/wireless/ath/ath11k/wmi.h | 15 ++++++++++
 drivers/net/wireless/ath/ath11k/wow.c | 53 +++++++++++++++++++++++++++++++++++
 3 files changed, 100 insertions(+)

diff --git a/drivers/net/wireless/ath/ath11k/wmi.c b/drivers/net/wireless/ath/ath11k/wmi.c
index f46e16f..e63ed58 100644
--- a/drivers/net/wireless/ath/ath11k/wmi.c
+++ b/drivers/net/wireless/ath/ath11k/wmi.c
@@ -6305,6 +6305,38 @@ void ath11k_wmi_detach(struct ath11k_base *ab)
 		ath11k_wmi_pdev_detach(ab, i);
 }
 
+int ath11k_send_hw_data_filter_cmd(struct ath11k *ar, u32 vdev_id,
+				   u32 filter_bitmap, bool enable)
+{
+	struct wmi_hw_data_filter_cmd *cmd;
+	struct sk_buff *skb;
+	int len;
+
+	len = sizeof(*cmd);
+	skb = ath11k_wmi_alloc_skb(ar->wmi->wmi_ab, len);
+
+	if (!skb)
+		return -ENOMEM;
+
+	cmd = (struct wmi_hw_data_filter_cmd *)skb->data;
+	cmd->tlv_header = FIELD_PREP(WMI_TLV_TAG, WMI_TAG_HW_DATA_FILTER_CMD) |
+			  FIELD_PREP(WMI_TLV_LEN, sizeof(*cmd) - TLV_HDR_SIZE);
+
+	cmd->vdev_id = vdev_id;
+	cmd->enable = enable;
+	/* Set all modes in case of disable */
+	if (!cmd->enable)
+		cmd->hw_filter_bitmap = ((u32)~0U);
+	else
+		cmd->hw_filter_bitmap = filter_bitmap;
+
+	ath11k_dbg(ar->ab, ATH11K_DBG_WMI,
+		   "wim set hw filter enable %d, filter_bitmap:0x%x\n",
+		   enable, filter_bitmap);
+
+	return ath11k_wmi_cmd_send(ar->wmi, skb, WMI_HW_DATA_FILTER_CMDID);
+}
+
 int ath11k_wmi_wow_enable(struct ath11k *ar)
 {
 	struct wmi_wow_enable_cmd *cmd;
diff --git a/drivers/net/wireless/ath/ath11k/wmi.h b/drivers/net/wireless/ath/ath11k/wmi.h
index 09cd9ef..d0708a7 100644
--- a/drivers/net/wireless/ath/ath11k/wmi.h
+++ b/drivers/net/wireless/ath/ath11k/wmi.h
@@ -4831,6 +4831,19 @@ enum wmi_wow_interface_cfg {
 	WOW_IFACE_PAUSE_DISABLED
 };
 
+/* Definition of HW data filtering */
+enum hw_data_filter_tupe {
+	WMI_HW_DATA_FILTER_DROP_NON_ARP_BC = 0x01,
+	WMI_HW_DATA_FILTER_DROP_NON_ICMPV6_MC = 0x02,
+};
+
+struct wmi_hw_data_filter_cmd {
+	u32 tlv_header;
+	u32 vdev_id;
+	u32 enable;
+	u32 hw_filter_bitmap;
+};
+
 /* WOW structures */
 enum wmi_wow_wakeup_event {
 	WOW_BMISS_EVENT = 0,
@@ -5379,4 +5392,6 @@ int ath11k_wmi_wow_add_wakeup_event(struct ath11k *ar, u32 vdev_id,
 				    enum wmi_wow_wakeup_event event,
 				    u32 enable);
 int ath11k_wmi_wow_enable(struct ath11k *ar);
+int ath11k_send_hw_data_filter_cmd(struct ath11k *ar, u32 vdev_id,
+				   u32 filter_bitmap, bool enable);
 #endif
diff --git a/drivers/net/wireless/ath/ath11k/wow.c b/drivers/net/wireless/ath/ath11k/wow.c
index 7ba2758..cc50455 100644
--- a/drivers/net/wireless/ath/ath11k/wow.c
+++ b/drivers/net/wireless/ath/ath11k/wow.c
@@ -444,6 +444,48 @@ static int ath11k_wow_nlo_cleanup(struct ath11k *ar)
 	return 0;
 }
 
+static int ath11k_wow_set_hw_filter(struct ath11k *ar)
+{
+	struct ath11k_vif *arvif;
+	int ret = 0;
+
+	lockdep_assert_held(&ar->conf_mutex);
+
+	list_for_each_entry(arvif, &ar->arvifs, list) {
+		ret = ath11k_send_hw_data_filter_cmd(ar, arvif->vdev_id,
+						     WMI_HW_DATA_FILTER_DROP_NON_ICMPV6_MC |
+						     WMI_HW_DATA_FILTER_DROP_NON_ARP_BC,
+			true);
+		if (ret) {
+			ath11k_warn(ar->ab, "failed to set hw data filter on vdev %i: %d\n",
+				    arvif->vdev_id, ret);
+			return ret;
+		}
+	}
+
+	return 0;
+}
+
+static int ath11k_wow_clear_hw_filter(struct ath11k *ar)
+{
+	struct ath11k_vif *arvif;
+	int ret = 0;
+
+	lockdep_assert_held(&ar->conf_mutex);
+
+	list_for_each_entry(arvif, &ar->arvifs, list) {
+		ret = ath11k_send_hw_data_filter_cmd(ar, arvif->vdev_id, 0, false);
+
+		if (ret) {
+			ath11k_warn(ar->ab, "failed to clear hw data filter on vdev %i: %d\n",
+				    arvif->vdev_id, ret);
+			return ret;
+		}
+	}
+
+	return 0;
+}
+
 static int ath11k_wow_enable(struct ath11k *ar)
 {
 	int i, ret;
@@ -529,6 +571,13 @@ int ath11k_wow_op_suspend(struct ieee80211_hw *hw,
 
 	ath11k_mac_drain_tx(ar);
 
+	ret = ath11k_wow_set_hw_filter(ar);
+	if (ret) {
+		ath11k_warn(ar->ab, "failed to set hw filter: %d\n",
+			    ret);
+		goto cleanup;
+	}
+
 	ret = ath11k_wow_enable(ar);
 	if (ret) {
 		ath11k_warn(ar->ab, "failed to start wow: %d\n", ret);
@@ -584,6 +633,10 @@ int ath11k_wow_op_resume(struct ieee80211_hw *hw)
 	if (ret)
 		ath11k_warn(ar->ab, "failed to cleanup nlo: %d\n", ret);
 
+	ret = ath11k_wow_clear_hw_filter(ar);
+	if (ret)
+		ath11k_warn(ar->ab, "failed to clear hw filter: %d\n", ret);
+
 exit:
 	if (ret) {
 		switch (ar->state) {
-- 
2.7.4

