From 2fae9de353e73d3480f83d71f65dae7fa9964654 Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Tue, 3 Dec 2019 16:49:00 +0200
Subject: [PATCH 048/124] ath11k: attach hw parameter

QCA6390 has a few different behaviors from IPQ8074 and the difference
will be defined in hw param. Attach hw param based on hw rev.

Tested-on: QCA6390 WLAN.HST.1.0.1-01230-QCAHSTSWPLZ_V2_TO_X86-1

Change-Id: I55cd53a4ee2d9dd0f71885f80cb4753b50625968
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
Signed-off-by: Kalle Valo <kvalo@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/core.c | 54 +++++++++++++++++++++++++++++-----
 drivers/net/wireless/ath/ath11k/hw.h   |  1 +
 2 files changed, 47 insertions(+), 8 deletions(-)

diff --git a/drivers/net/wireless/ath/ath11k/core.c b/drivers/net/wireless/ath/ath11k/core.c
index 79fffcfd..c6e8015 100644
--- a/drivers/net/wireless/ath/ath11k/core.c
+++ b/drivers/net/wireless/ath/ath11k/core.c
@@ -18,13 +18,20 @@ EXPORT_SYMBOL(ath11k_debug_mask);
 module_param_named(debug_mask, ath11k_debug_mask, uint, 0644);
 MODULE_PARM_DESC(debug_mask, "Debugging mask");
 
-static const struct ath11k_hw_params ath11k_hw_params = {
-	.name = "ipq8074",
-	.fw = {
-		.dir = IPQ8074_FW_DIR,
-		.board_size = IPQ8074_MAX_BOARD_DATA_SZ,
-		.cal_size =  IPQ8074_MAX_CAL_DATA_SZ,
+static const struct ath11k_hw_params ath11k_hw_params_list[] = {
+	{
+		.name = "ipq8074",
+		.dev_id = ATH11K_HW_IPQ8074,
+		.fw = {
+			.dir = IPQ8074_FW_DIR,
+			.board_size = IPQ8074_MAX_BOARD_DATA_SZ,
+			.cal_size =  IPQ8074_MAX_CAL_DATA_SZ,
+		},
 	},
+	{
+		.name = "qca6390",
+		.dev_id = ATH11K_HW_QCA6390,
+	}
 };
 
 /* Map from pdev index to hw mac index */
@@ -706,7 +713,33 @@ static void ath11k_core_restart(struct work_struct *work)
 	complete(&ab->driver_recovery);
 }
 
-static int ath11k_core_get_rproc_hdl (struct ath11k_base *ab)
+static int ath11k_init_hw_params(struct ath11k_base *ab)
+{
+	const struct ath11k_hw_params *hw_params = NULL;
+	int i;
+
+	for (i = 0; i < ARRAY_SIZE(ath11k_hw_params_list); i++) {
+		hw_params = &ath11k_hw_params_list[i];
+
+		if (hw_params->dev_id == ab->hw_rev)
+			break;
+	}
+
+	if (i == ARRAY_SIZE(ath11k_hw_params_list)) {
+		ath11k_err(ab,
+			   "Unsupported hardware version: 0x%x\n", ab->hw_rev);
+		return -EINVAL;
+	}
+
+	ab->hw_params = *hw_params;
+
+	ath11k_dbg(ab, ATH11K_DBG_BOOT, "Hardware name %s\n",
+		   ab->hw_params.name);
+
+	return 0;
+}
+
+static int ath11k_core_get_rproc_hdl(struct ath11k_base *ab)
 {
 	struct device *dev = ab->dev;
 	phandle rproc_phandle;
@@ -724,7 +757,6 @@ static int ath11k_core_get_rproc_hdl (struct ath11k_base *ab)
 	}
 
 	ab->tgt_rproc = prproc;
-	ab->hw_params = ath11k_hw_params;
 
 	return 0;
 }
@@ -736,6 +768,12 @@ int ath11k_core_init(struct ath11k_base *ab)
 	if (!ab->mhi_support)
 		ath11k_core_get_rproc_hdl(ab);
 
+	ret = ath11k_init_hw_params(ab);
+	if (ret) {
+		ath11k_err(ab, "failed to get hw params %d\n", ret);
+		return ret;
+	}
+
 	ret = ath11k_core_soc_create(ab);
 	if (ret) {
 		ath11k_err(ab, "failed to create soc core: %d\n", ret);
diff --git a/drivers/net/wireless/ath/ath11k/hw.h b/drivers/net/wireless/ath/ath11k/hw.h
index dc4434a..aee8c4b 100644
--- a/drivers/net/wireless/ath/ath11k/hw.h
+++ b/drivers/net/wireless/ath/ath11k/hw.h
@@ -106,6 +106,7 @@ enum ath11k_bus {
 
 struct ath11k_hw_params {
 	const char *name;
+	u16 dev_id;
 	struct {
 		const char *dir;
 		size_t board_size;
-- 
2.7.4

