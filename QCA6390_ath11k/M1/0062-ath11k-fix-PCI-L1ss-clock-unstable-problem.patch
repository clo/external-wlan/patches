From 0f9d7b9b8c7426e8425b16759dfa375afc50cbaf Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Fri, 3 Apr 2020 13:32:46 +0800
Subject: [PATCH 62/76] ath11k: fix PCI L1ss clock unstable problem

For QCA6390, one PCI related clock drifts sometimes, and
it makes PCI link difficult to quit L1ss. Fix it by writing
some registers which are suggested by hw team.

Tested QCA6390 on X86 platform.

Change-Id: I70c446458115e54a146aa323aef5f3ca3435e310
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/pci.c | 59 +++++++++++++++++++++++++++++++++++
 drivers/net/wireless/ath/ath11k/pci.h | 11 +++++++
 2 files changed, 70 insertions(+)

diff --git a/drivers/net/wireless/ath/ath11k/pci.c b/drivers/net/wireless/ath/ath11k/pci.c
index d3b722a..4ad5bc4 100644
--- a/drivers/net/wireless/ath/ath11k/pci.c
+++ b/drivers/net/wireless/ath/ath11k/pci.c
@@ -445,6 +445,64 @@ void ath11k_pci_clear_dbg_registers(struct ath11k_base *ab)
 	ath11k_dbg(ab, ATH11K_DBG_PCI, "soc reset cause:%d\n", val);
 }
 
+static bool ath11k_pci_set_link_reg(struct ath11k_base *ab,
+				    u32 offset, u32 value, u32 mask)
+{
+	u32 ret;
+	int count = 10;
+
+	ret = ath11k_pci_read32(ab, offset);
+	if ((ret & mask) == value)
+		return true;
+
+	while (count > 0) {
+		ath11k_pci_write32(ab, offset, (ret & ~mask) | value);
+		ret = ath11k_pci_read32(ab, offset);
+		if ((ret & mask) == value)
+			return true;
+
+		mdelay(2);
+		count--;
+	}
+
+	ath11k_err(ab, "%s: Failed to set Pcie Link Register 0x%08x to 0x%08x\n",
+		   __func__, offset, ret);
+	return false;
+}
+
+void ath11k_pci_fix_l1ss(struct ath11k_base *ab)
+{
+	if (!ath11k_pci_set_link_reg(ab,
+				     PCIE_QSERDES_COM_SYSCLK_EN_SEL_REG,
+				     PCIE_QSERDES_COM_SYSCLK_EN_SEL_VAL,
+				     PCIE_QSERDES_COM_SYSCLK_EN_SEL_MSK)) {
+		ath11k_err(ab, "%s set sysclk error\n", __func__);
+		return;
+	}
+
+	if (!ath11k_pci_set_link_reg(ab,
+				     PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG1_REG,
+				     PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG1_VAL,
+				     PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG_MSK)) {
+		ath11k_err(ab, "%s set dtct_config1 error\n", __func__);
+		return;
+	}
+
+	if (!ath11k_pci_set_link_reg(ab,
+				     PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG2_REG,
+				     PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG2_VAL,
+				     PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG_MSK)) {
+		ath11k_err(ab, "%s set dtct_config2 error\n", __func__);
+		return;
+	}
+
+	if (!ath11k_pci_set_link_reg(ab,
+				     PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG4_REG,
+				     PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG4_VAL,
+				     PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG_MSK))
+		ath11k_err(ab, "%s set dtct_config4 error\n", __func__);
+}
+
 /* Below is a WAR to avoid HST PCIE interface drop during HotReset
  * plus Global reset. According to HW desinger, need to enable
  * Debug Bit then can do reset.
@@ -537,6 +595,7 @@ void ath11k_pci_sw_reset(struct ath11k_base *ab, bool power_on)
 		ath11k_pic_enable_host_reset(ab);
 		ath11k_pci_enable_LTSSM(ab);
 		ath11k_pci_clear_all_intrs(ab);
+		ath11k_pci_fix_l1ss(ab);
 	} else {
 		ath11k_pci_host_reset(ab);
 		ath11k_pci_soc_global_reset(ab);
diff --git a/drivers/net/wireless/ath/ath11k/pci.h b/drivers/net/wireless/ath/ath11k/pci.h
index 01f5d11..58a1315 100644
--- a/drivers/net/wireless/ath/ath11k/pci.h
+++ b/drivers/net/wireless/ath/ath11k/pci.h
@@ -47,6 +47,17 @@
 #define PCIE_SMLH_REQ_RST_LINK_DOWN       0x2
 #define PCIE_INT_CLEAR_ALL                0xFFFFFFFF
 
+#define PCIE_QSERDES_COM_SYSCLK_EN_SEL_REG      0x01E0C0AC
+#define PCIE_QSERDES_COM_SYSCLK_EN_SEL_VAL      0x10
+#define PCIE_QSERDES_COM_SYSCLK_EN_SEL_MSK      0xFFFFFFFF
+#define PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG1_REG 0x01E0C628
+#define PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG1_VAL 0x02
+#define PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG2_REG 0x01E0C62C
+#define PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG2_VAL 0x52
+#define PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG4_REG 0x01E0C634
+#define PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG4_VAL 0xFF
+#define PCIE_USB3_PCS_MISC_OSC_DTCT_CONFIG_MSK  0x000000FF
+
 /* Register to wake the UMAC from power collapse */
 #define PCIE_SCRATCH_0_SOC_PCIE_REG 0x4040
 /* Register used for handshake mechanism to validate UMAC is awake */
-- 
2.7.4

