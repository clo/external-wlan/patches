From 8f4cf7fa3a3f5690ddf912cb0c2918143e4b7709 Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Thu, 26 Mar 2020 13:53:07 +0800
Subject: [PATCH 085/124] ath11k: read and write registers below unwindowed
 address

For QCA6390, host can read and write regsiters below unwindowed
address directly without programming the window register. For
registers below bar0 + 4k - 32, host can read and write regardless
of the power save state. Shadow registers are located below
bar0 + 4K - 32.

Tested-on: QCA6390 WLAN.HST.1.0.1-01230-QCAHSTSWPLZ_V2_TO_X86-1

Change-Id: I55570f3129da3706afc6df434bb52087c9f75ec9
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/core.c |  2 ++
 drivers/net/wireless/ath/ath11k/hw.h   |  1 +
 drivers/net/wireless/ath/ath11k/mhi.c  |  9 +++++++++
 drivers/net/wireless/ath/ath11k/mhi.h  |  2 ++
 drivers/net/wireless/ath/ath11k/pci.c  | 36 ++++++++++++++++++++++++++++------
 drivers/net/wireless/ath/ath11k/pci.h  |  6 ++++++
 6 files changed, 50 insertions(+), 6 deletions(-)

diff --git a/drivers/net/wireless/ath/ath11k/core.c b/drivers/net/wireless/ath/ath11k/core.c
index 47dc469..7c14648 100644
--- a/drivers/net/wireless/ath/ath11k/core.c
+++ b/drivers/net/wireless/ath/ath11k/core.c
@@ -35,6 +35,7 @@ static const struct ath11k_hw_params ath11k_hw_params_list[] = {
 		.rxdma1_enable = true,
 		.num_rxmda_per_pdev = 1,
 		.vdev_start_delay = false,
+		.wakeup_mhi = false,
 	},
 	{
 		.name = "qca6390",
@@ -47,6 +48,7 @@ static const struct ath11k_hw_params ath11k_hw_params_list[] = {
 		.rxdma1_enable = false,
 		.num_rxmda_per_pdev = 2,
 		.vdev_start_delay = true,
+		.wakeup_mhi = true,
 	}
 };
 
diff --git a/drivers/net/wireless/ath/ath11k/hw.h b/drivers/net/wireless/ath/ath11k/hw.h
index cf82b9a..d37d8f8 100644
--- a/drivers/net/wireless/ath/ath11k/hw.h
+++ b/drivers/net/wireless/ath/ath11k/hw.h
@@ -139,6 +139,7 @@ struct ath11k_hw_params {
 	bool rxdma1_enable;
 	int num_rxmda_per_pdev;
 	bool vdev_start_delay;
+	bool wakeup_mhi;
 };
 
 struct ath11k_fw_ie {
diff --git a/drivers/net/wireless/ath/ath11k/mhi.c b/drivers/net/wireless/ath/ath11k/mhi.c
index c60898d..1f67646 100644
--- a/drivers/net/wireless/ath/ath11k/mhi.c
+++ b/drivers/net/wireless/ath/ath11k/mhi.c
@@ -430,3 +430,12 @@ void ath11k_pci_force_mhi_rddm(struct ath11k_pci *ab_pci)
 	ath11k_pci_set_mhi_state(ab_pci, ATH11K_MHI_TRIGGER_RDDM);
 }
 
+void ath11k_pci_wakeup_mhi(struct ath11k_pci *ar_pci)
+{
+	mhi_device_get_sync(ar_pci->mhi_ctrl->mhi_dev, MHI_VOTE_DEVICE);
+}
+
+void ath11k_pci_release_mhi(struct ath11k_pci *ar_pci)
+{
+	mhi_device_put(ar_pci->mhi_ctrl->mhi_dev, MHI_VOTE_DEVICE);
+}
diff --git a/drivers/net/wireless/ath/ath11k/mhi.h b/drivers/net/wireless/ath/ath11k/mhi.h
index b9cc309..ab8bc80 100644
--- a/drivers/net/wireless/ath/ath11k/mhi.h
+++ b/drivers/net/wireless/ath/ath11k/mhi.h
@@ -28,4 +28,6 @@ void ath11k_pci_unregister_mhi(struct ath11k_pci *ar_pci);
 void ath11k_pci_suspend_mhi(struct ath11k_pci *ar_pci);
 void ath11k_pci_resume_mhi(struct ath11k_pci *ar_pci);
 void ath11k_pci_force_mhi_rddm(struct ath11k_pci *ar_pci);
+void ath11k_pci_wakeup_mhi(struct ath11k_pci *ar_pci);
+void ath11k_pci_release_mhi(struct ath11k_pci *ar_pci);
 #endif
diff --git a/drivers/net/wireless/ath/ath11k/pci.c b/drivers/net/wireless/ath/ath11k/pci.c
index 65d120a..6e6edd0 100644
--- a/drivers/net/wireless/ath/ath11k/pci.c
+++ b/drivers/net/wireless/ath/ath11k/pci.c
@@ -331,14 +331,26 @@ static void ath11k_pci_write32(struct ath11k_base *ab, u32 offset, u32 value)
 {
 	struct ath11k_pci *ab_pci = ath11k_pci_priv(ab);
 
-	if (ab->use_register_windowing) {
+	/* for offset beyond BAR + 4K - 32, may
+	 * need to wakeup MHI to access.
+	 */
+	if (ab->hw_params.wakeup_mhi &&
+	    offset >= ACCESS_ALWAYS_OFF)
+		ath11k_pci_wakeup_mhi(ab_pci);
+
+	if (!ab->use_register_windowing ||
+	    offset < MAX_UNWINDOWED_ADDRESS)
+		iowrite32(value, ab_pci->mem  + offset);
+	else {
 		spin_lock_bh(&ab_pci->window_lock);
 		ath11k_pci_select_window(ab_pci, offset);
 		iowrite32(value, ab_pci->mem + WINDOW_START + (offset & WINDOW_RANGE_MASK));
 		spin_unlock_bh(&ab_pci->window_lock);
-	} else {
-		iowrite32(value, ab_pci->mem  + offset);
 	}
+
+	if (ab->hw_params.wakeup_mhi &&
+	    offset >= ACCESS_ALWAYS_OFF)
+		ath11k_pci_release_mhi(ab_pci);
 }
 
 static u32 ath11k_pci_read32(struct ath11k_base *ab, u32 offset)
@@ -346,15 +358,27 @@ static u32 ath11k_pci_read32(struct ath11k_base *ab, u32 offset)
 	struct ath11k_pci *ab_pci = ath11k_pci_priv(ab);
 	u32 val;
 
-	if (ab->use_register_windowing) {
+	/* for offset beyond BAR + 4K - 32, may
+	 * need to wakeup MHI to access.
+	 */
+	if (ab->hw_params.wakeup_mhi &&
+	    offset >= ACCESS_ALWAYS_OFF)
+		ath11k_pci_wakeup_mhi(ab_pci);
+
+	if (!ab->use_register_windowing ||
+	    offset < MAX_UNWINDOWED_ADDRESS)
+		val = ioread32(ab_pci->mem + offset);
+	else {
 		spin_lock_bh(&ab_pci->window_lock);
 		ath11k_pci_select_window(ab_pci, offset);
 		val = ioread32(ab_pci->mem + WINDOW_START + (offset & WINDOW_RANGE_MASK));
 		spin_unlock_bh(&ab_pci->window_lock);
-	} else {
-		val = ioread32(ab_pci->mem + offset);
 	}
 
+	if (ab->hw_params.wakeup_mhi &&
+	    offset >= ACCESS_ALWAYS_OFF)
+		ath11k_pci_release_mhi(ab_pci);
+
 	return val;
 }
 
diff --git a/drivers/net/wireless/ath/ath11k/pci.h b/drivers/net/wireless/ath/ath11k/pci.h
index 6527fe7..ea80b50 100644
--- a/drivers/net/wireless/ath/ath11k/pci.h
+++ b/drivers/net/wireless/ath/ath11k/pci.h
@@ -23,6 +23,12 @@
 #define WINDOW_START MAX_UNWINDOWED_ADDRESS
 #define WINDOW_RANGE_MASK 0x7FFFF
 
+/* BAR0 + 4k is always accessible, and no
+ * need to force wakeup.
+ * 4K - 32 = 0xFE0
+ */
+#define ACCESS_ALWAYS_OFF 0xFE0
+
 #define ATH11K_IRQ_CE0_OFFSET 3
 
 struct ath11k_msi_user {
-- 
2.7.4

