From a47e41a3e0b99efd06750747d3260d4dd726eda2 Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Mon, 29 Jun 2020 16:04:11 +0800
Subject: [PATCH 154/173] ath11k: define different tx comp ring size for
 QCA6390

QCA6390 doesn't need such a big value of tx_comp_ring_size as
IPQ8074, so define a smaller value for QCA6390.

Considering other parameters which may need different values for
different chips, so define ath11k_hw_srng_params and different
chip can use this to define its' own specific values.

Tested-on: QCA6390 hw2.0 PCI WLAN.HST.1.0.1-01740-QCAHSTSWPLZ_V2_TO_X86-1

Change-Id: I2f2df51f0a0f4ca9892bf193ae92c2080a752228
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>

Conflicts:
	drivers/net/wireless/ath/ath11k/core.c
	drivers/net/wireless/ath/ath11k/hw.h
---
 drivers/net/wireless/ath/ath11k/core.c  |  2 ++
 drivers/net/wireless/ath/ath11k/dp.c    |  6 +++---
 drivers/net/wireless/ath/ath11k/dp.h    |  6 +++---
 drivers/net/wireless/ath/ath11k/dp_tx.c | 12 ++++++------
 drivers/net/wireless/ath/ath11k/hw.c    |  8 ++++++++
 drivers/net/wireless/ath/ath11k/hw.h    |  8 ++++++++
 6 files changed, 30 insertions(+), 12 deletions(-)

diff --git a/drivers/net/wireless/ath/ath11k/core.c b/drivers/net/wireless/ath/ath11k/core.c
index 40b27f9d..5308032 100644
--- a/drivers/net/wireless/ath/ath11k/core.c
+++ b/drivers/net/wireless/ath/ath11k/core.c
@@ -32,6 +32,7 @@
 		.hw_ops = &ath11k_hw_ops_ipq8074,
 		.misc_caps = MISC_CAPS_BAND_TO_MAC |
 			     MISC_CAPS_V2_MAP,
+		.srng_params = &ath11k_hw_srng_ipq8074,
 		.rxdma1_enable = true,
 		.num_rxmda_per_pdev = 1,
 		.vdev_start_delay = false,
@@ -52,6 +53,7 @@
 		.misc_caps = MISC_CAPS_HOST2FW_RXBUF_RING |
 			     MISC_CAPS_TCL_0_ONLY |
 			     MISC_CAPS_IDLE_PS_SUPP,
+		.srng_params = &ath11k_hw_srng_qca6390,
 		.rxdma1_enable = false,
 		.num_rxmda_per_pdev = 2,
 		.vdev_start_delay = true,
diff --git a/drivers/net/wireless/ath/ath11k/dp.c b/drivers/net/wireless/ath/ath11k/dp.c
index 9375e84..605df29 100644
--- a/drivers/net/wireless/ath/ath11k/dp.c
+++ b/drivers/net/wireless/ath/ath11k/dp.c
@@ -397,7 +397,7 @@ static int ath11k_dp_srng_common_setup(struct ath11k_base *ab)
 
 		ret = ath11k_dp_srng_setup(ab, &dp->tx_ring[i].tcl_comp_ring,
 					   HAL_WBM2SW_RELEASE, i, 0,
-					   DP_TX_COMP_RING_SIZE);
+					   DP_TX_COMP_RING_SIZE(ab));
 		if (ret) {
 			ath11k_warn(ab, "failed to set up tcl_comp ring ring (%d) :%d\n",
 				    i, ret);
@@ -1077,7 +1077,7 @@ int ath11k_dp_alloc(struct ath11k_base *ab)
 	if (ret)
 		goto fail_link_desc_cleanup;
 
-	size = sizeof(struct hal_wbm_release_ring) * DP_TX_COMP_RING_SIZE;
+	size = sizeof(struct hal_wbm_release_ring) * DP_TX_COMP_RING_SIZE(ab);
 
 	for (i = 0; i < DP_TCL_NUM_RING_MAX; i++) {
 		idr_init(&dp->tx_ring[i].txbuf_idr);
@@ -1085,7 +1085,7 @@ int ath11k_dp_alloc(struct ath11k_base *ab)
 		dp->tx_ring[i].tcl_data_ring_id = i;
 
 		dp->tx_ring[i].tx_status_head = 0;
-		dp->tx_ring[i].tx_status_tail = DP_TX_COMP_RING_SIZE - 1;
+		dp->tx_ring[i].tx_status_tail = DP_TX_COMP_RING_SIZE(ab) - 1;
 		dp->tx_ring[i].tx_status = kmalloc(size, GFP_KERNEL);
 		if (!dp->tx_ring[i].tx_status) {
 			ret = -ENOMEM;
diff --git a/drivers/net/wireless/ath/ath11k/dp.h b/drivers/net/wireless/ath/ath11k/dp.h
index b55c881..0fc4cf9 100644
--- a/drivers/net/wireless/ath/ath11k/dp.h
+++ b/drivers/net/wireless/ath/ath11k/dp.h
@@ -77,7 +77,7 @@ struct dp_rxdma_ring {
 	int bufs_max;
 };
 
-#define ATH11K_TX_COMPL_NEXT(x)	(((x) + 1) % DP_TX_COMP_RING_SIZE)
+#define ATH11K_TX_COMPL_NEXT(ab, x)	(((x) + 1) % DP_TX_COMP_RING_SIZE(ab))
 
 struct dp_tx_ring {
 	u8 tcl_data_ring_id;
@@ -178,8 +178,8 @@ struct ath11k_pdev_dp {
 
 #define DP_WBM_RELEASE_RING_SIZE	64
 #define DP_TCL_DATA_RING_SIZE		512
-#define DP_TX_COMP_RING_SIZE		32768
-#define DP_TX_IDR_SIZE			DP_TX_COMP_RING_SIZE
+#define DP_TX_COMP_RING_SIZE(ab)	((ab)->hw_params.srng_params->tx_comp_ring_size)
+#define DP_TX_IDR_SIZE(ab)		DP_TX_COMP_RING_SIZE(ab)
 #define DP_TCL_CMD_RING_SIZE		32
 #define DP_TCL_STATUS_RING_SIZE		32
 #define DP_REO_DST_RING_MAX		4
diff --git a/drivers/net/wireless/ath/ath11k/dp_tx.c b/drivers/net/wireless/ath/ath11k/dp_tx.c
index b580cba..7dd6ef0 100644
--- a/drivers/net/wireless/ath/ath11k/dp_tx.c
+++ b/drivers/net/wireless/ath/ath11k/dp_tx.c
@@ -123,7 +123,7 @@ int ath11k_dp_tx(struct ath11k *ar, struct ath11k_vif *arvif,
 
 	spin_lock_bh(&tx_ring->tx_idr_lock);
 	ret = idr_alloc(&tx_ring->txbuf_idr, skb, 0,
-			DP_TX_IDR_SIZE - 1, GFP_ATOMIC);
+			DP_TX_IDR_SIZE(ab) - 1, GFP_ATOMIC);
 	spin_unlock_bh(&tx_ring->tx_idr_lock);
 
 	if (ret < 0) {
@@ -521,29 +521,29 @@ void ath11k_dp_tx_completion_handler(struct ath11k_base *ab, int ring_id)
 
 	ath11k_hal_srng_access_begin(ab, status_ring);
 
-	while ((ATH11K_TX_COMPL_NEXT(tx_ring->tx_status_head) !=
+	while ((ATH11K_TX_COMPL_NEXT(ab, tx_ring->tx_status_head) !=
 		tx_ring->tx_status_tail) &&
 	       (desc = ath11k_hal_srng_dst_get_next_entry(ab, status_ring))) {
 		memcpy(&tx_ring->tx_status[tx_ring->tx_status_head],
 		       desc, sizeof(struct hal_wbm_release_ring));
 		tx_ring->tx_status_head =
-			ATH11K_TX_COMPL_NEXT(tx_ring->tx_status_head);
+			ATH11K_TX_COMPL_NEXT(ab, tx_ring->tx_status_head);
 	}
 
 	if ((ath11k_hal_srng_dst_peek(ab, status_ring) != NULL) &&
-	    (ATH11K_TX_COMPL_NEXT(tx_ring->tx_status_head) == tx_ring->tx_status_tail)) {
+	    (ATH11K_TX_COMPL_NEXT(ab, tx_ring->tx_status_head) == tx_ring->tx_status_tail)) {
 		/* TODO: Process pending tx_status messages when kfifo_is_full() */
 		ath11k_warn(ab, "Unable to process some of the tx_status ring desc because status_fifo is full\n");
 	}
 
 	ath11k_hal_srng_access_end(ab, status_ring);
 
-	while (ATH11K_TX_COMPL_NEXT(tx_ring->tx_status_tail) != tx_ring->tx_status_head) {
+	while (ATH11K_TX_COMPL_NEXT(ab, tx_ring->tx_status_tail) != tx_ring->tx_status_head) {
 		struct hal_wbm_release_ring *tx_status;
 		u32 desc_id;
 
 		tx_ring->tx_status_tail =
-			ATH11K_TX_COMPL_NEXT(tx_ring->tx_status_tail);
+			ATH11K_TX_COMPL_NEXT(ab, tx_ring->tx_status_tail);
 		tx_status = &tx_ring->tx_status[tx_ring->tx_status_tail];
 		ath11k_dp_tx_status_parse(ab, tx_status, &ts);
 
diff --git a/drivers/net/wireless/ath/ath11k/hw.c b/drivers/net/wireless/ath/ath11k/hw.c
index 9596e51..b4fe637 100644
--- a/drivers/net/wireless/ath/ath11k/hw.c
+++ b/drivers/net/wireless/ath/ath11k/hw.c
@@ -242,3 +242,11 @@ int ath11k_mac_id_to_srng_id_qca6x90(int mac_id)
 	.mac_id_to_pdev_id = ath11k_mac_id_to_pdev_id_ipq8074,
 	.mac_id_to_srng_id = ath11k_mac_id_to_srng_id_ipq8074,
 };
+
+const struct ath11k_hw_srng_params ath11k_hw_srng_ipq8074 = {
+	.tx_comp_ring_size = 32768,
+};
+
+const struct ath11k_hw_srng_params ath11k_hw_srng_qca6390 = {
+	.tx_comp_ring_size = 1024,
+};
diff --git a/drivers/net/wireless/ath/ath11k/hw.h b/drivers/net/wireless/ath/ath11k/hw.h
index 91db557..8e055b9 100644
--- a/drivers/net/wireless/ath/ath11k/hw.h
+++ b/drivers/net/wireless/ath/ath11k/hw.h
@@ -122,6 +122,10 @@ struct ath11k_hw_ops {
 
 };
 
+struct ath11k_hw_srng_params {
+	u32 tx_comp_ring_size;
+};
+
 struct ath11k_hw_params {
 	const char *name;
 	u16 dev_id;
@@ -138,6 +142,7 @@ struct ath11k_hw_params {
 	 * misc_caps is for these differences.
 	 */
 	u32 misc_caps;
+	const struct ath11k_hw_srng_params *srng_params;
 	bool rxdma1_enable;
 	int num_rxmda_per_pdev;
 	bool vdev_start_delay;
@@ -221,4 +226,7 @@ struct ath11k_hw_values {
 	bool rfkill_on_level;
 };
 
+extern const struct ath11k_hw_srng_params ath11k_hw_srng_ipq8074;
+extern const struct ath11k_hw_srng_params ath11k_hw_srng_qca6390;
+
 #endif
-- 
1.9.1

