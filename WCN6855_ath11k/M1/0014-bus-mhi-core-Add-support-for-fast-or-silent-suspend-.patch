From 9d03f1dff890ca67121b2caeebbac36a1559d0d2 Mon Sep 17 00:00:00 2001
From: Bhaumik Bhatt <bbhatt@codeaurora.org>
Date: Wed, 6 May 2020 17:57:44 -0700
Subject: [PATCH 014/124] bus: mhi: core: Add support for fast or silent
 suspend and resume

Add support for autonomous low power modes on the MHI host driver which
enables the controller to perform a fast or silent suspend and resume by
saving and restoring the device states.

If the endpoint supports this feature, the host can take advantage of
power savings by moving MHI state directly from M0 to a host-specific M3
state known as M3_fast without turning off the physical link. The device
can move to an M2 state without host acknowledgment, eliminating wait
times and transitioning to a lower power state faster as a result. Allow
the controller to decide when to notify clients of low power mode entry.

Ensure that a synchronous device vote also triggers a host wakeup to
support this feature.

Change-Id: I3a566b0b5e56a9b74c47406abfa53a5cd3f6dc2c
Signed-off-by: Bhaumik Bhatt <bbhatt@codeaurora.org>
---
 drivers/bus/mhi/core/pm.c | 193 ++++++++++++++++++++++++++++++++++++++++++++--
 include/linux/mhi.h       |  18 +++++
 2 files changed, 205 insertions(+), 6 deletions(-)

diff --git a/drivers/bus/mhi/core/pm.c b/drivers/bus/mhi/core/pm.c
index 4407338..6633360 100644
--- a/drivers/bus/mhi/core/pm.c
+++ b/drivers/bus/mhi/core/pm.c
@@ -791,6 +791,114 @@ int mhi_pm_suspend(struct mhi_controller *mhi_cntrl)
 }
 EXPORT_SYMBOL_GPL(mhi_pm_suspend);
 
+int mhi_pm_fast_suspend(struct mhi_controller *mhi_cntrl, bool notify_clients)
+{
+	struct mhi_chan *itr, *tmp;
+	struct mhi_device *mhi_dev = mhi_cntrl->mhi_dev;
+	struct device *dev = &mhi_dev->dev;
+	enum mhi_pm_state new_state;
+	int ret;
+
+	if (mhi_cntrl->pm_state == MHI_PM_DISABLE)
+		return -EINVAL;
+
+	if (MHI_PM_IN_ERROR_STATE(mhi_cntrl->pm_state))
+		return -EIO;
+
+	/* check if host/clients have any bus votes or packets to be sent */
+	if (atomic_read(&mhi_cntrl->pending_pkts) ||
+	    atomic_read(&mhi_dev->bus_vote))
+		return -EBUSY;
+
+	/* disable primary event ring processing to prevent interference */
+	tasklet_disable(&mhi_cntrl->mhi_event->task);
+
+	write_lock_irq(&mhi_cntrl->pm_lock);
+
+	/* re-check if host/clients have any bus votes or packets to be sent */
+	if (atomic_read(&mhi_cntrl->pending_pkts) ||
+	    atomic_read(&mhi_dev->bus_vote)) {
+		ret = -EBUSY;
+		goto error_suspend;
+	}
+
+	/* re-check to make sure no error has occurred before proceeding */
+	if (MHI_PM_IN_ERROR_STATE(mhi_cntrl->pm_state)) {
+		ret = -EIO;
+		goto error_suspend;
+	}
+
+	dev_dbg(dev, "Allowing Fast M3 transition with notify: %s\n",
+		notify_clients ? "true" : "false");
+
+	/* save the current states */
+	mhi_cntrl->saved_pm_state = mhi_cntrl->pm_state;
+	mhi_cntrl->saved_dev_state = mhi_cntrl->dev_state;
+
+	/* move from M2 to M0 as device can allow the transition but not host */
+	if (mhi_cntrl->pm_state == MHI_PM_M2) {
+		new_state = mhi_tryset_pm_state(mhi_cntrl, MHI_PM_M0);
+		if (new_state != MHI_PM_M0) {
+			dev_err(dev, "Error setting to PM state: %s from: %s\n",
+				to_mhi_pm_state_str(MHI_PM_M0),
+				to_mhi_pm_state_str(mhi_cntrl->pm_state));
+			ret = -EIO;
+			goto error_suspend;
+		}
+	}
+
+	new_state = mhi_tryset_pm_state(mhi_cntrl, MHI_PM_M3_ENTER);
+	if (new_state != MHI_PM_M3_ENTER) {
+		dev_err(dev, "Error setting to PM state: %s from: %s\n",
+			to_mhi_pm_state_str(MHI_PM_M3_ENTER),
+			to_mhi_pm_state_str(mhi_cntrl->pm_state));
+		ret = -EIO;
+		goto error_suspend;
+	}
+
+	/* set dev_state to M3_FAST and host pm_state to M3 */
+	new_state = mhi_tryset_pm_state(mhi_cntrl, MHI_PM_M3);
+	if (new_state != MHI_PM_M3) {
+		dev_err(dev, "Error setting to PM state: %s from: %s\n",
+			to_mhi_pm_state_str(MHI_PM_M3),
+			to_mhi_pm_state_str(mhi_cntrl->pm_state));
+		ret = -EIO;
+		goto error_suspend;
+	}
+
+	mhi_cntrl->dev_state = MHI_STATE_M3_FAST;
+	mhi_cntrl->M3_fast++;
+
+	write_unlock_irq(&mhi_cntrl->pm_lock);
+
+	/* enable primary event ring processing and check for events */
+	tasklet_enable(&mhi_cntrl->mhi_event->task);
+	mhi_irq_handler(0, mhi_cntrl->mhi_event);
+
+	/* Notify clients about entering LPM */
+	if (notify_clients) {
+		list_for_each_entry_safe(itr, tmp, &mhi_cntrl->lpm_chans,
+					 node) {
+			mutex_lock(&itr->mutex);
+			if (itr->mhi_dev)
+				mhi_notify(itr->mhi_dev, MHI_CB_LPM_ENTER);
+			mutex_unlock(&itr->mutex);
+		}
+	}
+
+	return 0;
+
+error_suspend:
+	write_unlock_irq(&mhi_cntrl->pm_lock);
+
+	/* enable primary event ring processing and check for events */
+	tasklet_enable(&mhi_cntrl->mhi_event->task);
+	mhi_irq_handler(0, mhi_cntrl->mhi_event);
+
+	return ret;
+}
+EXPORT_SYMBOL_GPL(mhi_pm_fast_suspend);
+
 int mhi_pm_resume(struct mhi_controller *mhi_cntrl)
 {
 	struct mhi_chan *itr, *tmp;
@@ -848,20 +956,90 @@ int mhi_pm_resume(struct mhi_controller *mhi_cntrl)
 }
 EXPORT_SYMBOL_GPL(mhi_pm_resume);
 
+int mhi_pm_fast_resume(struct mhi_controller *mhi_cntrl, bool notify_clients)
+{
+	struct mhi_chan *itr, *tmp;
+	struct device *dev = &mhi_cntrl->mhi_dev->dev;
+
+	dev_info(dev, "Entered with PM state: %s, MHI state: %s notify: %s\n",
+		 to_mhi_pm_state_str(mhi_cntrl->pm_state),
+		 TO_MHI_STATE_STR(mhi_cntrl->dev_state),
+		 notify_clients ? "true" : "false");
+
+	if (mhi_cntrl->pm_state == MHI_PM_DISABLE)
+		return 0;
+
+	if (MHI_PM_IN_ERROR_STATE(mhi_cntrl->pm_state))
+		return -EIO;
+
+	read_lock_bh(&mhi_cntrl->pm_lock);
+	WARN_ON(mhi_cntrl->pm_state != MHI_PM_M3);
+	read_unlock_bh(&mhi_cntrl->pm_lock);
+
+	/* Notify clients about exiting LPM */
+	if (notify_clients) {
+		list_for_each_entry_safe(itr, tmp, &mhi_cntrl->lpm_chans,
+					 node) {
+			mutex_lock(&itr->mutex);
+			if (itr->mhi_dev)
+				mhi_notify(itr->mhi_dev, MHI_CB_LPM_EXIT);
+			mutex_unlock(&itr->mutex);
+		}
+	}
+
+	/* disable primary event ring processing to prevent interference */
+	tasklet_disable(&mhi_cntrl->mhi_event->task);
+
+	write_lock_irq(&mhi_cntrl->pm_lock);
+
+	/* re-check to make sure no error has occurred before proceeding */
+	if (MHI_PM_IN_ERROR_STATE(mhi_cntrl->pm_state)) {
+		write_unlock_irq(&mhi_cntrl->pm_lock);
+		tasklet_enable(&mhi_cntrl->mhi_event->task);
+		return -EIO;
+	}
+
+	/* restore the states */
+	mhi_cntrl->pm_state = mhi_cntrl->saved_pm_state;
+	mhi_cntrl->dev_state = mhi_cntrl->saved_dev_state;
+
+	write_unlock_irq(&mhi_cntrl->pm_lock);
+
+	switch (mhi_cntrl->pm_state) {
+	case MHI_PM_M0:
+		mhi_pm_m0_transition(mhi_cntrl);
+		break;
+	case MHI_PM_M2:
+		read_lock_bh(&mhi_cntrl->pm_lock);
+		mhi_cntrl->wake_get(mhi_cntrl, true);
+		mhi_cntrl->wake_put(mhi_cntrl, true);
+		read_unlock_bh(&mhi_cntrl->pm_lock);
+		break;
+	default:
+		dev_err(dev, "Unexpected PM state:%s after restore\n",
+			to_mhi_pm_state_str(mhi_cntrl->pm_state));
+	}
+
+	/* enable primary event ring processing and check for events */
+	tasklet_enable(&mhi_cntrl->mhi_event->task);
+	mhi_irq_handler(0, mhi_cntrl->mhi_event);
+
+	return 0;
+}
+EXPORT_SYMBOL_GPL(mhi_pm_fast_resume);
+
 int __mhi_device_get_sync(struct mhi_controller *mhi_cntrl)
 {
 	int ret;
 
-	/* Wake up the device */
+	/* wake up the host and the device */
 	read_lock_bh(&mhi_cntrl->pm_lock);
 	mhi_cntrl->wake_get(mhi_cntrl, true);
-	if (MHI_PM_IN_SUSPEND_STATE(mhi_cntrl->pm_state)) {
-		pm_wakeup_event(&mhi_cntrl->mhi_dev->dev, 0);
-		mhi_cntrl->runtime_get(mhi_cntrl);
-		mhi_cntrl->runtime_put(mhi_cntrl);
-	}
 	read_unlock_bh(&mhi_cntrl->pm_lock);
 
+	pm_wakeup_dev_event(&mhi_cntrl->mhi_dev->dev, 0, false);
+	mhi_cntrl->runtime_get(mhi_cntrl);
+
 	ret = wait_event_timeout(mhi_cntrl->state_event,
 				 mhi_cntrl->pm_state == MHI_PM_M0 ||
 				 MHI_PM_IN_ERROR_STATE(mhi_cntrl->pm_state),
@@ -871,9 +1049,12 @@ int __mhi_device_get_sync(struct mhi_controller *mhi_cntrl)
 		read_lock_bh(&mhi_cntrl->pm_lock);
 		mhi_cntrl->wake_put(mhi_cntrl, false);
 		read_unlock_bh(&mhi_cntrl->pm_lock);
+
+		mhi_cntrl->runtime_put(mhi_cntrl);
 		return -EIO;
 	}
 
+	mhi_cntrl->runtime_put(mhi_cntrl);
 	return 0;
 }
 
diff --git a/include/linux/mhi.h b/include/linux/mhi.h
index 045b321..2790c51 100644
--- a/include/linux/mhi.h
+++ b/include/linux/mhi.h
@@ -331,9 +331,11 @@ struct mhi_controller_config {
  * @pm_lock: Lock for protecting MHI power management state
  * @timeout_ms: Timeout in ms for state transitions
  * @pm_state: MHI power management state
+ * @saved_pm_state: MHI power management state backup used in fast/silent modes
  * @db_access: DB access states
  * @ee: MHI device execution environment
  * @dev_state: MHI device state
+ * @saved_dev_state: MHI device state backup used in fast/silent modes
  * @dev_wake: Device wakeup count
  * @pending_pkts: Pending packets for the controller
  * @M0, M2, M3, M3_fast: Counters to track number of device MHI state changes
@@ -415,9 +417,11 @@ struct mhi_controller {
 	rwlock_t pm_lock;
 	u32 timeout_ms;
 	u32 pm_state;
+	u32 saved_pm_state;
 	u32 db_access;
 	enum mhi_ee_type ee;
 	enum mhi_state dev_state;
+	enum mhi_state saved_dev_state;
 	atomic_t dev_wake;
 	atomic_t pending_pkts;
 	u32 M0, M2, M3, M3_fast;
@@ -636,12 +640,26 @@ void mhi_unprepare_after_power_down(struct mhi_controller *mhi_cntrl);
 int mhi_pm_suspend(struct mhi_controller *mhi_cntrl);
 
 /**
+ * mhi_pm_fast_suspend - Move MHI into a fast/silent suspended state
+ * @mhi_cntrl: MHI controller
+ * @notify_clients: if true, clients will be notified of the suspend transition
+ */
+int mhi_pm_fast_suspend(struct mhi_controller *mhi_cntrl, bool notify_clients);
+
+/**
  * mhi_pm_resume - Resume MHI from suspended state
  * @mhi_cntrl: MHI controller
  */
 int mhi_pm_resume(struct mhi_controller *mhi_cntrl);
 
 /**
+ * mhi_pm_resume - Resume MHI from a fast/silent suspended state
+ * @mhi_cntrl: MHI controller
+ * @notify_clients: if true, clients will be notified of the resume transition
+ */
+int mhi_pm_fast_resume(struct mhi_controller *mhi_cntrl, bool notify_clients);
+
+/**
  * mhi_download_rddm_img - Download ramdump image from device for
  *                         debugging purpose.
  * @mhi_cntrl: MHI controller
-- 
2.7.4

