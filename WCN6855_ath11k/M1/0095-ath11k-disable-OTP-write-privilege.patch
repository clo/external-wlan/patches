From 50de11b78f9b8d4f9b9255e6fbc2993d250a391e Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Fri, 3 Apr 2020 13:42:13 +0800
Subject: [PATCH 095/124] ath11k: disable OTP write privilege

In stress test on QCA6390, it's found that sometimes the
OTP content is corrupt by unknown electric signals. This
change is to protect OTP by disabling the OTP write privilege.

Tested-on: QCA6390 WLAN.HST.1.0.1-01230-QCAHSTSWPLZ_V2_TO_X86-1

Change-Id: Ia81f115d7661021d3949b44c0fbf4236c9e9aad9
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/pci.c | 20 ++++++++++++++++++++
 drivers/net/wireless/ath/ath11k/pci.h |  4 ++++
 2 files changed, 24 insertions(+)

diff --git a/drivers/net/wireless/ath/ath11k/pci.c b/drivers/net/wireless/ath/ath11k/pci.c
index d6b421c..38ecfe5 100644
--- a/drivers/net/wireless/ath/ath11k/pci.c
+++ b/drivers/net/wireless/ath/ath11k/pci.c
@@ -577,6 +577,25 @@ static void ath11k_pci_clear_all_intrs(struct ath11k_base *ab)
 	ath11k_pci_write32(ab, PCIE_PCIE_INT_ALL_CLEAR, PCIE_INT_CLEAR_ALL);
 }
 
+static void ath11k_pci_set_wlaon_pwr_ctrl(struct ath11k_base *ab, bool set_vdd4blow)
+{
+	u32 val;
+
+	/* control FW otp write privilege.
+	 * Disable the write prvilige to protect otp from
+	 * overwritten by unknown electric signals. This issue
+	 * is seen in stress test.
+	 */
+	val = ath11k_pci_read32(ab, WLAON_QFPROM_PWR_CTRL_REG);
+
+	if (set_vdd4blow)
+		val |= QFPROM_PWR_CTRL_VDD4BLOW_MASK;
+	else
+		val &= ~QFPROM_PWR_CTRL_VDD4BLOW_MASK;
+
+	ath11k_pci_write32(ab, WLAON_QFPROM_PWR_CTRL_REG, val);
+}
+
 void ath11k_pci_force_wake(struct ath11k_base *ab)
 {
 	ath11k_pci_write32(ab, PCIE_SOC_WAKE_PCIE_LOCAL_REG, 1);
@@ -595,6 +614,7 @@ void ath11k_pci_sw_reset(struct ath11k_base *ab, bool power_on)
 		ath11k_pic_enable_host_reset(ab);
 		ath11k_pci_enable_LTSSM(ab);
 		ath11k_pci_clear_all_intrs(ab);
+		ath11k_pci_set_wlaon_pwr_ctrl(ab, false);
 		ath11k_pci_fix_l1ss(ab);
 	} else {
 		ath11k_pci_host_reset(ab);
diff --git a/drivers/net/wireless/ath/ath11k/pci.h b/drivers/net/wireless/ath/ath11k/pci.h
index 58a1315..75e5d30 100644
--- a/drivers/net/wireless/ath/ath11k/pci.h
+++ b/drivers/net/wireless/ath/ath11k/pci.h
@@ -47,6 +47,10 @@
 #define PCIE_SMLH_REQ_RST_LINK_DOWN       0x2
 #define PCIE_INT_CLEAR_ALL                0xFFFFFFFF
 
+#define WLAON_QFPROM_PWR_CTRL_REG         0x01F8031C
+#define QFPROM_PWR_CTRL_VDD4BLOW_MASK     0x4
+#define QFPROM_PWR_CTRL_SHUTDOWN_MASK     0x1
+
 #define PCIE_QSERDES_COM_SYSCLK_EN_SEL_REG      0x01E0C0AC
 #define PCIE_QSERDES_COM_SYSCLK_EN_SEL_VAL      0x10
 #define PCIE_QSERDES_COM_SYSCLK_EN_SEL_MSK      0xFFFFFFFF
-- 
2.7.4

