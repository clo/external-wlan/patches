From ec7c57afa49b2b68029ea88403f2a41133adbc63 Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Mon, 8 Jun 2020 16:57:54 +0800
Subject: [PATCH 119/124] ath11k: dump sram if firmware bootup fails

If firmware can't download or can't get into amss state, dump sram
to analyze further why failure happens.

Change-Id: I9a8fd001f2da0116724c145ecf287c0d9435b842
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/core.c  |  6 ++++
 drivers/net/wireless/ath/ath11k/debug.c | 59 +++++++++++++++++++++++++++++++++
 drivers/net/wireless/ath/ath11k/debug.h |  6 ++++
 3 files changed, 71 insertions(+)

diff --git a/drivers/net/wireless/ath/ath11k/core.c b/drivers/net/wireless/ath/ath11k/core.c
index ba6b91a..3e2031b 100644
--- a/drivers/net/wireless/ath/ath11k/core.c
+++ b/drivers/net/wireless/ath/ath11k/core.c
@@ -393,6 +393,12 @@ static int ath11k_core_soc_create(struct ath11k_base *ab)
 		goto err_qmi_deinit;
 	}
 
+	ret = ath11k_debug_sram_dump_create(ab);
+	if (ret) {
+		ath11k_err(ab, "failed to create ath11k sram dump debugfs\n");
+		goto err_debugfs_reg;
+	}
+
 	ret = ath11k_hif_power_up(ab);
 	if (ret) {
 		ath11k_err(ab, "failed to power up :%d\n", ret);
diff --git a/drivers/net/wireless/ath/ath11k/debug.c b/drivers/net/wireless/ath/ath11k/debug.c
index 9bb1e544..205e210 100644
--- a/drivers/net/wireless/ath/ath11k/debug.c
+++ b/drivers/net/wireless/ath/ath11k/debug.c
@@ -11,6 +11,10 @@
 #include "dp_tx.h"
 #include "debug_htt_stats.h"
 #include "peer.h"
+#include "hif.h"
+
+#define FW_SRAM_START_QCA6390   0x01400000
+#define FW_SRAM_END_QCA6390     0x0171ffff
 
 void ath11k_info(struct ath11k_base *ab, const char *fmt, ...)
 {
@@ -873,6 +877,61 @@ void ath11k_debug_pdev_destroy(struct ath11k_base *ab)
 	ab->debugfs_soc = NULL;
 }
 
+static int ath11k_open_sram_dump(struct inode *inode, struct file *file)
+{
+	struct ath11k_base *ab = inode->i_private;
+	char *buf;
+	int i;
+	int data;
+
+	buf = vmalloc(FW_SRAM_END_QCA6390 - FW_SRAM_START_QCA6390 + 4);
+	if (!buf)
+		return -ENOMEM;
+
+	file->private_data = buf;
+
+	for (i = FW_SRAM_START_QCA6390; i < FW_SRAM_END_QCA6390; i += 4) {
+		data = ath11k_hif_read32(ab, i);
+		memcpy(buf, &data, 4);
+		buf += 4;
+	}
+
+	return 0;
+}
+
+static ssize_t ath11k_read_sram_dump(struct file *file,
+				     char __user *user_buf,
+				     size_t count, loff_t *ppos)
+{
+	const char *buf = file->private_data;
+	int len = FW_SRAM_END_QCA6390 - FW_SRAM_START_QCA6390;
+
+	return simple_read_from_buffer(user_buf, count, ppos, buf, len);
+}
+
+static int ath11k_release_sram_dump(struct inode *inode, struct file *file)
+{
+	vfree(file->private_data);
+	file->private_data = NULL;
+
+	return 0;
+}
+
+static const struct file_operations fops_sram_dump = {
+	.open = ath11k_open_sram_dump,
+	.read = ath11k_read_sram_dump,
+	.release = ath11k_release_sram_dump,
+	.owner = THIS_MODULE,
+	.llseek = default_llseek,
+};
+
+int ath11k_debug_sram_dump_create(struct ath11k_base *ab)
+{
+	debugfs_create_file("sram", 0600, ab->debugfs_ath11k, ab,
+			    &fops_sram_dump);
+	return 0;
+}
+
 int ath11k_debug_soc_create(struct ath11k_base *ab)
 {
 	ab->debugfs_ath11k = debugfs_create_dir("ath11k", NULL);
diff --git a/drivers/net/wireless/ath/ath11k/debug.h b/drivers/net/wireless/ath/ath11k/debug.h
index 9b6c72e..54381e2 100644
--- a/drivers/net/wireless/ath/ath11k/debug.h
+++ b/drivers/net/wireless/ath/ath11k/debug.h
@@ -165,6 +165,7 @@ void ath11k_debug_fw_stats_process(struct ath11k_base *ab, struct sk_buff *skb);
 
 void ath11k_debug_fw_stats_init(struct ath11k *ar);
 int ath11k_dbg_htt_stats_req(struct ath11k *ar);
+int ath11k_debug_sram_dump_create(struct ath11k_base *ab);
 
 static inline bool ath11k_debug_is_pktlog_lite_mode_enabled(struct ath11k *ar)
 {
@@ -302,6 +303,11 @@ ath11k_update_per_peer_stats_from_txcompl(struct ath11k *ar,
 {
 }
 
+static inline int ath11k_debug_sram_dump_create(struct ath11k_base *ab)
+{
+	return 0;
+}
+
 #endif /* CONFIG_MAC80211_DEBUGFS*/
 
 #define ath11k_dbg(ar, dbg_mask, fmt, ...)			\
-- 
2.7.4

