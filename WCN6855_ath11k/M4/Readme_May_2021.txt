=================================================Code fetch================================================================================

1)	Clone code base: git clone https://git.kernel.org/pub/scm/linux/kernel/git/kvalo/ath.git -b master

2)	Reset to given tag: git checkout ath-202012180905

3)	Get patches from patchwork.kernel.org as below
	1. https://patchwork.kernel.org/project/linux-wireless/patch/20210511162214.29475-2-jouni@codeaurora.org/
	2. https://patchwork.kernel.org/project/linux-wireless/patch/20210511162214.29475-3-jouni@codeaurora.org/
	3. https://patchwork.kernel.org/project/linux-wireless/patch/20210511162214.29475-4-jouni@codeaurora.org/
	4. https://patchwork.kernel.org/project/linux-wireless/patch/20210511162214.29475-5-jouni@codeaurora.org/
	5. https://patchwork.kernel.org/project/linux-wireless/patch/20210511162214.29475-6-jouni@codeaurora.org/
	6. https://patchwork.kernel.org/project/linux-wireless/patch/20210511162214.29475-7-jouni@codeaurora.org/
	7. https://patchwork.kernel.org/project/linux-wireless/patch/20210511162214.29475-8-jouni@codeaurora.org/
	8. https://patchwork.kernel.org/project/linux-arm-msm/patch/20210420035339.282963-1-bqiang@codeaurora.org/
	9. https://patchwork.kernel.org/project/linux-wireless/patch/1609816120-9411-2-git-send-email-wgong@codeaurora.org/
	10. https://patchwork.kernel.org/project/linux-wireless/patch/1609816120-9411-3-git-send-email-wgong@codeaurora.org/
	
4)	Get patches from git.kernel.org as below
	1. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/drivers/net/wireless/ath/ath11k?h=mac80211-for-net-2021-05-11&id=4b965be536eefdd16ca0a88120fee23f5b92cd16
	2. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/drivers/net/wireless/ath/ath11k?h=mac80211-for-net-2021-05-11&id=7a3aed0c3c36cc08a1b123d752f141797f6ba79a
	3. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/drivers/net/wireless/ath/ath11k?h=mac80211-for-net-2021-05-11&id=16001e4b2e681b8fb5e7bc50db5522081d46347a
	4. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/drivers/net/wireless/ath/ath11k?h=mac80211-for-net-2021-05-11&id=fa5f473d764398a09f7deea3a042a1130ee50e90
	5. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/drivers/net/wireless/ath/ath11k?h=mac80211-for-net-2021-05-11&id=5f67d306155e6a757f0b6b2b061e3ea13f44c536
	6. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/drivers/net/wireless/ath/ath11k?h=mac80211-for-net-2021-05-11&id=a233811ef60081192a2b13ce23253671114308d8
	7. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/drivers/net/wireless/ath/ath11k?h=mac80211-for-net-2021-05-11&id=480a73610c95511e42fb7d0359b523f66883e51a
	8. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/drivers/net/wireless/ath/ath11k?h=mac80211-for-net-2021-05-11&id=6fe6f68fef7f7d5f6b5b62fde78de91cdc528c58
	9. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/drivers/net/wireless/ath/ath11k?h=mac80211-for-net-2021-05-11&id=e678fbd401b9bdca9d1bd64065abfcc87ae66b94
	10. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/drivers/net/wireless/ath/ath11k?h=mac80211-for-net-2021-05-11&id=cf8480d338a1b9156121e5e035e6b9721db4332a
	11. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/drivers/net/wireless/ath/ath11k?h=mac80211-for-net-2021-05-11&id=6289ac2b7182d418ee68e5c0f3f83d383d7a72ed
	12. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/drivers/net/wireless/ath/ath11k?h=mac80211-for-net-2021-05-11&id=7dc67af063e3f0237c864504bb2188ada753b804
	13. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/drivers/net/wireless/ath/ath11k?h=mac80211-for-net-2021-05-11&id=4e80946197a83a6115e308334618449b77696d6a
	14. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/?h=mac80211-for-net-2021-05-11&id=965a7d72e798eb7af0aa67210e37cf7ecd1c9cad
	15. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/?h=mac80211-for-net-2021-05-11&id=94034c40ab4a3fcf581fbc7f8fdf4e29943c4a24
	16. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/?h=mac80211-for-net-2021-05-11&id=a1d5ff5651ea592c67054233b14b30bf4452999c
	17. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/?h=mac80211-for-net-2021-05-11&id=2b8a1fee3488c602aca8bea004a087e60806a5cf
	18. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/?h=mac80211-for-net-2021-05-11&id=270032a2a9c4535799736142e1e7c413ca7b836e
	19. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/?h=mac80211-for-net-2021-05-11&id=3a11ce08c45b50d69c891d71760b7c5b92074709
	20. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/?h=mac80211-for-net-2021-05-11&id=bf30ca922a0c0176007e074b0acc77ed345e9990
	21. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/?h=mac80211-for-net-2021-05-11&id=7e44a0b597f04e67eee8cdcbe7ee706c6f5de38b
	22. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/?h=mac80211-for-net-2021-05-11&id=a8c4d76a8dd4fb9666fc8919a703d85fb8f44ed8
	23. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/?h=mac80211-for-net-2021-05-11&id=c3944a5621026c176001493d48ee66ff94e1a39a
	24. https://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211.git/commit/?h=mac80211-for-net-2021-05-11&id=210f563b097997ce917e82feab356b298bfd12b0
	25. https://git.kernel.org/pub/scm/linux/kernel/git/kvalo/ath.git/commit/drivers/net/wireless/ath/ath11k?id=ff9f732a87caa5f7bab72bea3aaad58db9b1ac60
	26. https://git.kernel.org/pub/scm/linux/kernel/git/kvalo/ath.git/commit/drivers/net/wireless/ath/ath11k?id=5d18b8a04ba2fd000475411737857995ecf70c9f
	27. https://git.kernel.org/pub/scm/linux/kernel/git/kvalo/ath.git/commit/drivers/net/wireless/ath/ath11k?id=096b625fab8f9d88d9b436288a64b70080219d4b
	28. https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=556bbb442bbb44f429dbaa9f8b48e0b4cda6e088
	29.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=a03c7a86e12721da9f6bb509dddda19fd9ae8c6c
	30.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=0c76b3fa580da8f65333b8774205f0a72ffe844c
	31. https://git.kernel.org/pub/scm/linux/kernel/git/bluetooth/bluetooth-next.git/commit/?id=3b0d5250be30e76727284bb9e4c26b662ede7378

5)	Get patches from codeaurora.org as below
	1.	git clone https://source.codeaurora.org/external/sba/wlan_patches
	2.	get patches from: <workspace>/wlan_patches/WCN6855_ath11k/M4
	
6)	Apply the patches of step 3)/4)/5) in below order one by one, total 110 patches. Please note the bottom is the first.
	ath11k: add hardware parameter for VHT beformee STS  capability
	ath11k: check msdu len in dp rx path
	ath11k: change HAL_RX_BUF_RBM_SW3_BM to HAL_RX_BUF_RBM_SW1_BM
	ath11k: change DP_TCL_NUM_RING_MAX to 1
	ath11k: change DMA_FROM_DEVICE to DMA_TO_DEVICE when map reinjected pkt
	ath11k: fix double increase on pending mgmt tx pkt
	ath11k: add sram start and end addr to hw params
	ath11k: advertise PLATFORM_CAP_PCIE_GLOBAL_RESET in qmi msg
	ath11k: add 5 seconds for timeout of scan started if both 6G and 11d scan offload enable
	ath11k: add trace log support from vnaralas(Venkateswara Naralasetty)
	ath11k: calucate the correct nss of peer for HE capabilities
	ath11k: fix the value of msecs_to_jiffies in ath11k_debugfs_fw_stats_request
	ath11k: set correct NL80211_FEATURE_DYNAMIC_SMPS for WCN6855
	ath11k: enable HE-160MHz and VHT-160MHz bandwidth for WCN6855
	ath11k: treat scan dequeued as aborting and scan complete
	ath11k: report tx bitrate for iw wlan station dump
	ath11k: remove IEEE80211_HW_USES_RSS flag for QCA6390
	ath11k: fix read fail for htt_stats and htt_peer_stats for single pdev
	ath11k: move peer delete after vdev stop of STATION for QCA6390
	ath11k: add ieee80211_unregister_hw to avoid kernel crash caused by NULL pointer
	ath11k: fix blocked for more than 120 seconds caused by reg update
	ath11k: change to copy cap info of 6G band under WMI_HOST_WLAN_5G_CAP for WCN685X
	ath11k: enable 6G channels for WCN685X
	ath11k: re-enable ht_cap/vht_cap for 5G band for WCN685X
	ath11k: [fix conflict in nl80211.c]add 6ghz params in peer assoc command
	ath11k: remove scan wmi for "ath11k: fix for accepting bcast presp in GHz scan "
	ath11k: fix for accepting bcast presp in GHz scan
	ath11k: change to indicate scan complete for scan canceled
	ath11k: Add signal report to mac80211 for QCA6390 and WCN685X
	ath11k: set scan state to abort if scan not started while restart
	ath11k: change to treat alpha code na as world regdomain
	ath11k: add hw-restart for simulate_fw_crash
	ath11k: report rssi of each chain to mac80211
	ath11k: remove return for empty tx bitrate in mac_op_sta_statistics
	ath11k: add wait opeartion for tx management packets for flush from mac80211
	ath11k: add support for device recovery for QCA6390
	ath11k: add support for hardware rfkill
	ath11k: enable pkt log default for QCA6390
	ath11k: add regdb.bin download for regdb offload
	ath11k: add 11d scan offload support
	ath11k: add handler for WMI_SET_CURRENT_COUNTRY_CMDID
	ath11k: skip sending vdev down for channel switch
	ath11k: add hw connection monitor support
	ath11k: add wmi op version indication for UTF
	ath11k: change check from ATH11K_STATE_ON to ATH11K_STATE_TM for UTF
	ath11k: add support for UTF mode for QCA6390
	ath11k: remove ATH11K_STATE_TM check for restart
	ath11k: factory test mode support from WIN team v6
	ath11k: change some dp ring parameters
	ath11k: enable idle power save in ath11k_core_qmi_firmware_ready
	ath11k: force wake target before mhi stop
	ath11k: Adjust the dest entries of some Copy Engines
	ath11k: decrease clients to 16 from 64 for QCA6390 series
	ath11k: decrease MHI IPC inbound entries and buffer length
	ath11k: fix deadloop in ath11k_dp_tx
	ath11k: don't send msi info to firmware for lmac rings
	ath11k: debug M3 download issue
	ath11k: define different tx comp ring size for QCA6390
	ath11k: fix memory leak of qmi event
	ath11k: fix invalid m3 buffer address
	ath11k: destroy workqueue when module is unloaded
	ath11k: dump sram if firmware bootup fails
	ath11k: set dtim policy to stick mode for station interface
	ath11k: support MAC address randomization in scan
	ath11k: support gtk rekey offload
	ath11k: support arp and ns offload
	ath11k: purge rx pktlog when entering WoW
	ath11k: implement hw data filter
	ath11k: add basic WoW functionality
	ath11k: Fix sounding dimension config in HE cap
	ath11k: Drop multicast fragments
	ath11k: Clear the fragment cache during key install
	ath11k: don't call ath11k_pci_set_l1ss for WCN6855
	ath11k: add support for WCN6855
	ath11k: add support to get peer id for WCN6855
	ath11k: setup WBM_IDLE_LINK ring once again
	ath11k: setup REO for WCN6855
	ath11k: add dp support for WCN6855
	ath11k: add hw reg support for WCN6855
	ath11k: pci: remove experimental warning
	ath11k: fix warning in ath11k_mhi_config
	ath11k: add qcn9074 pci device support
	ath11k: add extended interrupt support for QCN9074
	ath11k: add CE interrupt support for QCN9074
	ath11k: remove duplicate function declaration
	ath11k: add data path support for QCN9074
	ath11k: add hal support for QCN9074
	ath11k: add static window support for register access
	ath11k: Add qcn9074 mhi controller config
	ath11k: Update memory segment count for qcn9074
	ath11k: qmi: increase the number of fw segments
	ath11k: Move qmi service_ins_id to hw_params
	ath11k: Refactor ath11k_msi_config
	ath11k: Update tx descriptor search index properly
	Bluetooth: btusb: add shutdown function for wcn6855
	mac80211: do not accept/forward invalid EAPOL frames
	mac80211: prevent attacks on TKIP/WEP as well
	mac80211: check defrag PN against current frame
	mac80211: add fragment cache to sta_info
	mac80211: drop A-MSDUs on old ciphers
	cfg80211: mitigate A-MSDU aggregation attacks
	mac80211: properly handle A-MSDUs that start with an RFC 1042 header
	mac80211: prevent mixed key and fragment cache attacks
	mac80211: assure all fragments are encrypted
	mac80211: do intersection with he mcs and nss set of peer and own
	mac80211: remove NSS number of 160MHz if not support 160MHz for HE
	mhi: add MHI_STATE_M2 to resume success criteria
	bus: mhi: core: Prevent sending multiple RDDM entry callbacks
	bus: mhi: core: Mark and maintain device states early on after power down
	bus: mhi: core: Separate system error and power down handling

=================================================Compilation================================================================================

1)	make menuconfig and change config and save
	run cmd: make menuconfig and select the following 
	[M]Device Drivers ---> Bus devices ---> Modem Host Interface[CONFIG_MHI_BUS=m]
	[M]Device Drivers ---> SOC (System On Chip) specific Drivers ---> Qualcomm SoC drivers ---> Qualcomm qmi helpers[CONFIG_QCOM_QMI_HELPERS=m]
	[M]Networking support ---> Networking options ---> Qualcomm IPC Router support[CONFIG_QRTR=m]
	[M]Networking support ---> Networking options ---> MHI IPC Router channels[CONFIG_QRTR_MHI=m]
	[M]Device Drivers ---> Network device support ---> Wireless LAN ---> Qualcomm Technologies 802.11ax chipset support[CONFIG_ATH11K=m]
	[M]Device Drivers ---> Network device support ---> Wireless LAN ---> Qualcomm Technologies 802.11ax chipset PCI support[CONFIG_ATH11K_PCI=m]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> QCA ath11k debugging[CONFIG_ATH11K_DEBUG=y]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> QCA ath11k debugfs support[CONFIG_ATH11K_DEBUGFS=y]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> ath11k tracing support[CONFIG_ATH11K_TRACING=y]
	[*]Device Drivers ---> Character devices ---> Serial device bus
	[*]Networking support ---> Bluetooth subsystem support ---> Bluetooth device drivers ---> Qualcomm Atheros protocol support
	[*]Networking support ---> Wireless ---> cfg80211 certification onus[CONFIG_CFG80211_CERTIFICATION_ONUS=y]
	[*]Networking support ---> Wireless ---> nl80211 testmode command[CONFIG_NL80211_TESTMODE=y]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> Atheros dynamic user regulatory hints[CONFIG_ATH_REG_DYNAMIC_USER_REG_HINTS=y]

2)	To build the kernel follow the steps 
	1.	make
	2.	sudo make modules_install
	3.	sudo make install


======================================WLAN bring up=======================================================================   
 
1)	Copy firmware files
	1. find firmware from CE team
	2. Copy all firmware binary files to /lib/firmware/ath11k/WCN6855/hw2.0 and rename bd file 
	3. sudo cp amss.mbn /lib/firmware/ath11k/WCN6855/hw2.0/amss.bin 
	4. sudo cp bdwlan01.e06 /lib/firmware/ath11k/WCN6855/hw2.0/board.bin 
	5. sudo cp m3.bin  /lib/firmware/ath11k/WCN6855/hw2.0
    6. sudo cp regdb.bin  /lib/firmware/ath11k/WCN6855/hw2.0

 
2)	Load modules
	sudo modprobe cfg80211
	sudo modprobe mac80211

	cd /lib/modules/5.10.0-wt-ath+/kernel/drivers/bus/mhi/core
	sudo insmod mhi.ko
	
	cd /lib/modules/5.10.0-wt-ath+/kernel/net/qrtr
	sudo insmod ns.ko
	sudo insmod qrtr.ko
	sudo insmod qrtr-mhi.ko

	cd /lib/modules/5.10.0-wt-ath+/kernel/drivers/soc/qcom
	sudo insmod qmi_helpers.ko

	cd /lib/modules/5.10.0-wt-ath+/kernel/drivers/net/wireless/ath/ath11k
	sudo insmod ath11k.ko debug_mask=0xffffffff
	sudo insmod ath11k_pci.ko

3)	Use ifconfig to check whether the WiFi interface is up. If yes, try scan and connect AP from Ubuntu Network Manager GUI

4)	If wifi is loaded successfully, try Reboot system see whether wifi driver can be loaded automatically and successfully 

======================================Bluetooth bring up=======================================================================   
1)	Get BT firmware files
	
2)	Copy AthrBT_0x00130200.dfu, ramps_0x00130200.dfu, ramps_0x00130200_0104.dfu, ramps_0x00130200_0105.dfu, ramps_0x00130200_0106.dfu and ramps_0x00130200_0107.dfu into /lib/firmware/qca

3)	Reboot the DUT and BT should be workable now