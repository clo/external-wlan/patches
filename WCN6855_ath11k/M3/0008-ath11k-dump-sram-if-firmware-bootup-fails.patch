From 23ec6560e89f6528f0d691871b7d10d0b1c2b46b Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Mon, 8 Jun 2020 16:57:54 +0800
Subject: [PATCH 08/76] ath11k: dump sram if firmware bootup fails

If firmware can't download or can't get into amss state, dump sram
to analyze further why failure happens.

Tested-on: QCA6390 hw2.0 PCI WLAN.HST.1.0.1-01740-QCAHSTSWPLZ_V2_TO_X86-1

Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/core.c    |  6 ++++
 drivers/net/wireless/ath/ath11k/debug.c   |  2 +-
 drivers/net/wireless/ath/ath11k/debugfs.c | 56 +++++++++++++++++++++++++++++++
 drivers/net/wireless/ath/ath11k/debugfs.h |  9 ++++-
 4 files changed, 71 insertions(+), 2 deletions(-)

diff --git a/drivers/net/wireless/ath/ath11k/core.c b/drivers/net/wireless/ath/ath11k/core.c
index 6a5a363..df2aa7a 100644
--- a/drivers/net/wireless/ath/ath11k/core.c
+++ b/drivers/net/wireless/ath/ath11k/core.c
@@ -568,6 +568,12 @@ static int ath11k_core_soc_create(struct ath11k_base *ab)
 		goto err_qmi_deinit;
 	}
 
+	ret = ath11k_debug_sram_dump_create(ab);
+	if (ret) {
+		ath11k_err(ab, "failed to create ath11k sram dump debugfs\n");
+		goto err_debugfs_reg;
+	}
+
 	ret = ath11k_hif_power_up(ab);
 	if (ret) {
 		ath11k_err(ab, "failed to power up :%d\n", ret);
diff --git a/drivers/net/wireless/ath/ath11k/debug.c b/drivers/net/wireless/ath/ath11k/debug.c
index c86de95..df6fba9 100644
--- a/drivers/net/wireless/ath/ath11k/debug.c
+++ b/drivers/net/wireless/ath/ath11k/debug.c
@@ -103,4 +103,4 @@ void ath11k_dbg_dump(struct ath11k_base *ab,
 }
 EXPORT_SYMBOL(ath11k_dbg_dump);
 
-#endif /* CONFIG_ATH11K_DEBUG */
+#endif /* CONFIG_ATH11K_DEBUGFS */
diff --git a/drivers/net/wireless/ath/ath11k/debugfs.c b/drivers/net/wireless/ath/ath11k/debugfs.c
index 554feaf..6824610 100644
--- a/drivers/net/wireless/ath/ath11k/debugfs.c
+++ b/drivers/net/wireless/ath/ath11k/debugfs.c
@@ -12,6 +12,7 @@
 #include "dp_tx.h"
 #include "debugfs_htt_stats.h"
 #include "peer.h"
+#include "hif.h"
 
 static const char *htt_bp_umac_ring[HTT_SW_UMAC_RING_IDX_MAX] = {
 	"REO2SW1_RING",
@@ -1096,3 +1097,58 @@ int ath11k_debugfs_register(struct ath11k *ar)
 void ath11k_debugfs_unregister(struct ath11k *ar)
 {
 }
+
+static int ath11k_open_sram_dump(struct inode *inode, struct file *file)
+{
+	struct ath11k_base *ab = inode->i_private;
+	char *buf;
+	int i;
+	int data;
+
+	buf = vmalloc(FW_SRAM_END_QCA6390 - FW_SRAM_START_QCA6390 + 4);
+	if (!buf)
+		return -ENOMEM;
+
+	file->private_data = buf;
+
+	for (i = FW_SRAM_START_QCA6390; i < FW_SRAM_END_QCA6390; i += 4) {
+		data = ath11k_hif_read32(ab, i);
+		memcpy(buf, &data, 4);
+		buf += 4;
+	}
+
+	return 0;
+}
+
+static ssize_t ath11k_read_sram_dump(struct file *file,
+				     char __user *user_buf,
+				     size_t count, loff_t *ppos)
+{
+	const char *buf = file->private_data;
+	int len = FW_SRAM_END_QCA6390 - FW_SRAM_START_QCA6390;
+
+	return simple_read_from_buffer(user_buf, count, ppos, buf, len);
+}
+
+static int ath11k_release_sram_dump(struct inode *inode, struct file *file)
+{
+	vfree(file->private_data);
+	file->private_data = NULL;
+
+	return 0;
+}
+
+static const struct file_operations fops_sram_dump = {
+	.open = ath11k_open_sram_dump,
+	.read = ath11k_read_sram_dump,
+	.release = ath11k_release_sram_dump,
+	.owner = THIS_MODULE,
+	.llseek = default_llseek,
+};
+
+int ath11k_debug_sram_dump_create(struct ath11k_base *ab)
+{
+	debugfs_create_file("sram", 0600, ab->debugfs_ath11k, ab,
+			    &fops_sram_dump);
+	return 0;
+}
diff --git a/drivers/net/wireless/ath/ath11k/debugfs.h b/drivers/net/wireless/ath/ath11k/debugfs.h
index e5346af..df7bf4c 100644
--- a/drivers/net/wireless/ath/ath11k/debugfs.h
+++ b/drivers/net/wireless/ath/ath11k/debugfs.h
@@ -11,6 +11,9 @@
 #define ATH11K_TX_POWER_MAX_VAL	70
 #define ATH11K_TX_POWER_MIN_VAL	0
 
+#define FW_SRAM_START_QCA6390   0x01400000
+#define FW_SRAM_END_QCA6390     0x0171ffff
+
 /* htt_dbg_ext_stats_type */
 enum ath11k_dbg_htt_ext_stats_type {
 	ATH11K_DBG_HTT_EXT_STATS_RESET                      =  0,
@@ -111,7 +114,7 @@ void ath11k_debugfs_pdev_destroy(struct ath11k_base *ab);
 int ath11k_debugfs_register(struct ath11k *ar);
 void ath11k_debugfs_unregister(struct ath11k *ar);
 void ath11k_debugfs_fw_stats_process(struct ath11k_base *ab, struct sk_buff *skb);
-
+int ath11k_debug_sram_dump_create(struct ath11k_base *ab);
 void ath11k_debugfs_fw_stats_init(struct ath11k *ar);
 
 static inline bool ath11k_debugfs_is_pktlog_lite_mode_enabled(struct ath11k *ar)
@@ -212,6 +215,10 @@ static inline int ath11k_debugfs_rx_filter(struct ath11k *ar)
 	return 0;
 }
 
+int ath11k_debug_sram_dump_create(struct ath11k_base *ab)
+{
+	return 0;
+}
 #endif /* CONFIG_MAC80211_DEBUGFS*/
 
 #endif /* _ATH11K_DEBUGFS_H_ */
-- 
2.7.4

