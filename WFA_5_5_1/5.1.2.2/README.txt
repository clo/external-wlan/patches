======================================================
Instructions to apply patches given in this directory:
======================================================
After following instructions as per distributed Release Notes
perform following steps:

1)  Apply in vendor/qcom/opensource/wlan/qcacld-3.0 folder
    * 0001-qcacld-3.0-Release-5-1-2-2.patch: 

2)  Apply in device/qcom/msm8996 folder
    * 0001-Enable-interworking-bit-by-default-in-wpa_supplicant.patch:
    * 0001-qcacld-3.0-Update-WCNSS_qcom_cfg.ini-for-qcacld-3.0-.patch:
    * 0001-wlan-Make-qca_cld3-as-default-driver-to-load.patch:

3)  Edit android.config file in external/wpa_supplicant_8
    * android.config.additions.txt:
======================================================
