======================================================
Instructions to apply patches given in this directory:
======================================================
After following instructions as per distributed 5.1.2.4 
Release Notes perform following additional steps:

1)  Apply in vendor/qcom/opensource/wlan/qcacld-3.0 folder
    * 0001-qcacld-3.0-Release-5-1-2-4.patch

2)  Apply in device/qcom/msm8996 folder
    * 0001-Enable-interworking-bit-by-default-in-wpa_supplicant.patch
    * 0001-qcacld-3.0-Update-WCNSS_qcom_cfg.ini-with-NAN-for-qcacld-3.0.patch
    * 0001-wlan-Make-qca_cld3-as-default-driver-to-load.patch

3)  Apply in vendor/qcom/opensource/wlan/utils/sigma-dut folder
    * 0001-sigma-dut-Add-Support-for-NAN.patch

4)  Apply in hardware/qcom/wlan
    * 0001-WiFiHAL-Fast-forward-to-all-NAN-Changes-till-PF5.patch

5)  Apply in hardware/libhardware_legacy
    * 0001-NAN-Fast-forward-to-all-NAN-Changes-till-PF5.patch

4)  Edit android.config file in external/wpa_supplicant_8
    * android.config.additions.txt:
======================================================
