#!/bin/bash
#
# SCRIPT TO OVERRIDE PROJECTS IN A GIVEN MANIFEST .XML FILE
#
# Contact: Pdhavali@qti.qualcomm.com
#
# $Id: //depot/software/swbuild/bin/la_integ/utils/manifestUpdate.sh.sh#44 $
# $DateTime: 2017/04/05 09:37:42 $
# $Author: pdhavali $
#

input_file=$1
output_file="${input_file}.out"

# Array to hold projects that need to be replaced or added to defaut manifest.xml
declare -A replace

# Replace manifest entries for these projects
replace["platform/external/wpa_supplicant_8"]='<project name="hostap" remote="supp" path="external/wpa_supplicant_8" revision="1f3c49d418238e20a9eb256920b8f6fb149643bb" upstream="master"/>'

replace["platform/hardware/qcom/wlan"]='<project name="platform/hardware/qcom/wlan" remote="caf" path="hardware/qcom/wlan" revision="49bfe17446df8da0931862277b0e94d58ed19392" upstream="wlan-aosp.lnx.2.0.r25-rel"/>'

replace["platform/hardware/libhardware_legacy"]='<project name="platform/hardware/libhardware_legacy" remote="caf" path="hardware/libhardware_legacy" revision="ef27c599ff799255cd64d31cb9414ec6505275d6" upstream="android-framework.lnx.2.0.r16-rel"/>'

replace["platform/system/qcom"]='<project name="platform/system/qcom" remote="caf" path="vendor/qcom/opensource/softap" revision="f4eef25c34eb1710ff5abe5bb1f0bbe42747af03" upstream="wlan-service.lnx.1.1-rel"/>'

replace["platform/vendor/qcom-opensource/wlan/fw-api"]='<project name="platform/vendor/qcom-opensource/wlan/fw-api" remote="caf" path="vendor/qcom/opensource/wlan/fw-api" revision="67e0a7b4a7d28752053da96ca6a0b1e65487a54c" upstream="wlan-api.lnx.1.1-rel"/>'

replace["platform/vendor/qcom-opensource/wlan/qca-wifi-host-cmn"]='<project name="platform/vendor/qcom-opensource/wlan/qca-wifi-host-cmn" remote="caf" path="vendor/qcom/opensource/wlan/qca-wifi-host-cmn" revision="0bb1d6ca65069a48e51d61c1b1fb9ee685ee3515" upstream="wlan-cmn.driver.lnx.1.0-rel"/>'

replace["platform/vendor/qcom-opensource/wlan/qcacld-3.0"]='<project name="platform/vendor/qcom-opensource/wlan/qcacld-3.0" remote="caf" path="vendor/qcom/opensource/wlan/qcacld-3.0" revision="73ec30420d450841c9f4f8a67a0b0369a17ced9d" upstream="wlan-cld3.driver.lnx.1.1-rel"/>'

# replace["platform/vendor/qcom-opensource/wlan/utils/sigma-dut"]='<project name="qca/sigma-dut" remote="github" path="vendor/qcom/opensource/wlan/utils/sigma-dut" revision="4c8158fffdc0c3d3c63aefa46a100e61c2bc7674" upstream="master"/>'
# Fast forward sigma-dut to WPA3 change.
replace["platform/vendor/qcom-opensource/wlan/utils/sigma-dut"]='<project name="qca/sigma-dut" remote="github" path="vendor/qcom/opensource/wlan/utils/sigma-dut" revision="1165f172949f0cd8241197102046e0e4343702a5" upstream="master"/>'

tmpfile=$(mktemp /tmp/prjtmpXXXX)
rm -fv $output_file

while IFS= read -r line
do

	# Skip </manifest> until all lines are spit out
	if echo "$line" | egrep -q "</manifest>";
	then
		continue
	fi

	# Skip prima. Don't skip qcacld-2.0 because BSP has a patch for it.
	if echo "$line" | egrep -q "wlan/prima";
	then
		continue
	fi

	# Process only <project line and for others show them as-is
	if echo "$line" | egrep -qv "<project ";
	then
		echo "$line" >> $output_file
		continue
	fi

	linetmp=$(echo "$line" | sed -e 's/"//g')

	echo "$linetmp" | fmt -1 | grep name= | awk -F= '{print $2}' > $tmpfile
	project=$(cat $tmpfile | col -b)

	# If project needs to be replacen, skip it here
	if [ "${replace[$project]}" == "" ]; then
		echo "$line" >> $output_file
	fi
done < "$input_file"
rm -f $tmpfile

echo ' <!-- START: Qualcomm additional project mappings -->'             >> $output_file
echo ' <remote fetch="git://git-android.quicinc.com/" name="qcremote"/>' >> $output_file
echo ' <remote fetch="git://github.com/" name="github"/>'                >> $output_file
echo ' <remote fetch="git://w1.fi/" name="supp" />'                      >> $output_file

# Finally append the new projects that are not in CAF
for prj in ${!replace[@]}
do
	echo "  ${replace[$prj]}"                            >> $output_file
done
echo ' <!-- END  : Qualcomm additional project mappings -->' >> $output_file
echo "</manifest>"                                           >> $output_file

