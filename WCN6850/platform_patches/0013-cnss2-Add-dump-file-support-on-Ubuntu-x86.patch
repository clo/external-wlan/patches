From 707c2b69de6a9a48e3315034aad6f0ba6aaeb8b1 Mon Sep 17 00:00:00 2001
From: Lin Bai <lbai@codeaurora.org>
Date: Tue, 5 Mar 2019 16:20:01 +0800
Subject: [PATCH 13/14] cnss2: Add dump file support on Ubuntu/x86

Write dump files to file-system on Ubuntu
---
 cnss2/debug.c   |  3 +++
 cnss2/main.c    | 80 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++-
 cnss2/pci.c     | 20 +++++++++++++++
 cnss2/ramdump.h | 56 ++++++++++++++++++++++++++++++++++++++++
 4 files changed, 158 insertions(+), 1 deletion(-)
 create mode 100755 cnss2/ramdump.h

diff --git a/cnss2/debug.c b/cnss2/debug.c
index cd50949..97a6787 100755
--- a/cnss2/debug.c
+++ b/cnss2/debug.c
@@ -179,6 +179,8 @@ static ssize_t cnss_dev_boot_debug_write(struct file *fp,
 					     CNSS_DRIVER_EVENT_POWER_DOWN,
 					     CNSS_EVENT_SYNC, NULL);
 		clear_bit(CNSS_DRIVER_DEBUG, &plat_priv->driver_state);
+	} else if (sysfs_streq(cmd, "assert")) {
+		ret = cnss_force_fw_assert(&pci_priv->pci_dev->dev);
 	} else {
 		cnss_pr_err("Device boot debugfs command is invalid\n");
 		ret = -EINVAL;
@@ -202,6 +204,7 @@ static int cnss_dev_boot_debug_show(struct seq_file *s, void *data)
 	seq_puts(s, "linkdown: bring down PCIe link\n");
 	seq_puts(s, "powerup: full power on sequence to boot device, download FW and do QMI handshake with FW\n");
 	seq_puts(s, "shutdown: full power off sequence to shutdown device\n");
+	seq_puts(s, "assert: trigger firmware assert\n");
 
 	return 0;
 }
diff --git a/cnss2/main.c b/cnss2/main.c
index 28f6b2a..9281159 100755
--- a/cnss2/main.c
+++ b/cnss2/main.c
@@ -23,6 +23,7 @@
 #include <soc/qcom/ramdump.h>
 #include <soc/qcom/subsystem_notif.h>
 #else
+#include "ramdump.h"
 #endif
 #include "main.h"
 #include "debug.h"
@@ -72,6 +73,10 @@ unsigned long bd_file_type = 1;
 module_param(bd_file_type, ulong, 0600);
 MODULE_PARM_DESC(bd_file_type, "Board data file type.");
 
+static bool rddm_panic = 1;
+module_param(rddm_panic, bool, 0600);
+MODULE_PARM_DESC(rddm_panic, "Trigger kernel panic when RDDM happens");
+
 static struct cnss_fw_files FW_FILES_QCA6174_FW_3_0 = {
 	"qwlan30.bin", "bdwlan30.bin", "otp30.bin", "utf30.bin",
 	"utfbd30.bin", "epping30.bin", "evicted30.bin"
@@ -1548,7 +1553,10 @@ static int cnss_do_recovery(struct cnss_plat_data *plat_priv,
 			break;
 		}
 		cnss_pci_collect_dump_info(pci_priv);
-		break;
+		if (rddm_panic)
+			panic("cnss: RDDM triggers kernel panic");
+		else
+			goto self_recovery;
 	case CNSS_REASON_DEFAULT:
 	case CNSS_REASON_TIMEOUT:
 		break;
@@ -1703,6 +1711,12 @@ int cnss_force_fw_assert(struct device *dev)
 }
 EXPORT_SYMBOL(cnss_force_fw_assert);
 
+int cnss_force_collect_rddm(struct device *dev)
+{
+	return 0;
+}
+EXPORT_SYMBOL(cnss_force_collect_rddm);
+
 void fw_boot_timeout(unsigned long data)
 {
 	struct cnss_plat_data *plat_priv = (struct cnss_plat_data *)data;
@@ -1913,6 +1927,70 @@ static void cnss_driver_event_work(struct work_struct *work)
 }
 
 #ifdef CONFIG_NAPIER_X86
+static int cnss_panic_handler(struct notifier_block *this,
+				unsigned long event, void *ptr)
+{
+	struct cnss_plat_data *plat_priv = cnss_get_plat_priv(NULL);
+	struct cnss_pci_data *pci_priv = plat_priv->bus_priv;
+
+	cnss_crash_shutdown(NULL);
+	cnss_pci_shutdown(pci_priv->pci_dev);
+
+	return NOTIFY_DONE;
+}
+
+static struct notifier_block panic_nb = {
+	.notifier_call  = cnss_panic_handler,
+};
+
+int cnss_register_subsys(struct cnss_plat_data *plat_priv) {
+	return atomic_notifier_chain_register(&panic_notifier_list, &panic_nb);
+
+}
+void cnss_unregister_subsys(struct cnss_plat_data *plat_priv) {
+	atomic_notifier_chain_unregister(&panic_notifier_list, &panic_nb);
+}
+
+/*
+ * Implemented for compatibility with crashscope/MTP.
+ * Save dump paddr/vaddr/length in seg_table.
+ *
+ * 4KB mem allocate to store array of structure cnss_dump_seg:
+ * Risk: when num_entries > 4KB/sizeof(cnss_dump_seg).
+ */
+int cnss_register_ramdump(struct cnss_plat_data *plat_priv)
+{
+	struct cnss_ramdump_info_v2 *info_v2;
+	struct cnss_dump_data *dump_data;
+
+	info_v2 = &plat_priv->ramdump_info_v2;
+	dump_data = &info_v2->dump_data;
+
+	info_v2->dump_data_vaddr = kzalloc(CNSS_DUMP_DESC_SIZE, GFP_KERNEL);
+	if (!info_v2->dump_data_vaddr)
+		return -ENOMEM;
+
+	dump_data->paddr = virt_to_phys(info_v2->dump_data_vaddr);
+	dump_data->version = CNSS_DUMP_FORMAT_VER_V2;
+	dump_data->magic = CNSS_DUMP_MAGIC_VER_V2;
+	dump_data->seg_version = CNSS_DUMP_SEG_VER;
+	strlcpy(dump_data->name, CNSS_DUMP_NAME,
+		sizeof(dump_data->name));
+
+	return 0;
+}
+
+void cnss_unregister_ramdump(struct cnss_plat_data *plat_priv)
+{
+	struct cnss_ramdump_info_v2 *info_v2;
+
+	info_v2 = &plat_priv->ramdump_info_v2;
+
+	kfree(info_v2->dump_data_vaddr);
+	info_v2->dump_data_vaddr = NULL;
+	info_v2->dump_data_valid = false;
+}
+
 static int cnss_register_bus_scale(struct cnss_plat_data *plat_priv) {return 0;}
 static void cnss_unregister_bus_scale(struct cnss_plat_data *plat_priv) {}
 #else
diff --git a/cnss2/pci.c b/cnss2/pci.c
index 27cdd4d..b05a73e 100755
--- a/cnss2/pci.c
+++ b/cnss2/pci.c
@@ -1225,6 +1225,9 @@ void cnss_pci_collect_dump_info(struct cnss_pci_data *pci_priv)
 	struct cnss_dump_data *dump_data =
 		&plat_priv->ramdump_info_v2.dump_data;
 	void *start_addr, *end_addr;
+	struct cnss_fw_mem *fw_mem = plat_priv->fw_mem;
+	struct cnss_dump_seg *dump_seg;
+	int i;
 
 	dump_data->nentries = 0;
 
@@ -1236,6 +1239,23 @@ void cnss_pci_collect_dump_info(struct cnss_pci_data *pci_priv)
 	end_addr = cnss_pci_collect_dump_seg(pci_priv,
 					     MHI_RDDM_RD_SEGMENT, start_addr);
 
+	cnss_pr_dbg("Collect remote heap dump segment\n");
+	dump_seg = end_addr;
+
+	for (i = 0; i < plat_priv->fw_mem_seg_len; i++) {
+		if (fw_mem[i].type == QMI_WLFW_MEM_TYPE_DDR_V01) {
+			dump_seg->address = fw_mem[i].pa;
+			dump_seg->v_address = fw_mem[i].va;
+			dump_seg->size = fw_mem[i].size;
+			dump_seg->type = 2;
+			cnss_pr_dbg("seg-%d: address 0x%lx, v_address %pK, size 0x%lx\n",
+				    i, dump_seg->address, dump_seg->v_address,
+				    dump_seg->size);
+			dump_seg++;
+			dump_data->nentries++;
+		}
+	}
+
 	if (dump_data->nentries > 0)
 		plat_priv->ramdump_info_v2.dump_data_valid = true;
 }
diff --git a/cnss2/ramdump.h b/cnss2/ramdump.h
new file mode 100755
index 0000000..50a17c8
--- /dev/null
+++ b/cnss2/ramdump.h
@@ -0,0 +1,56 @@
+/* Copyright (c) 2011-2014, 2019 The Linux Foundation. All rights reserved.
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License version 2 and
+ * only version 2 as published by the Free Software Foundation.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ */
+
+#ifndef _RAMDUMP_HEADER
+#define _RAMDUMP_HEADER
+
+struct device;
+
+struct ramdump_segment {
+	unsigned long address;
+	void *v_address;
+	unsigned long size;
+};
+
+#ifdef CONFIG_MSM_SUBSYSTEM_RESTART
+extern void *create_ramdump_device(const char *dev_name, struct device *parent);
+extern void destroy_ramdump_device(void *dev);
+extern int do_ramdump(void *handle, struct ramdump_segment *segments,
+		int nsegments);
+extern int do_elf_ramdump(void *handle, struct ramdump_segment *segments,
+		int nsegments);
+
+#else
+static inline void *create_ramdump_device(const char *dev_name,
+		struct device *parent)
+{
+	return NULL;
+}
+
+static inline void destroy_ramdump_device(void *dev)
+{
+}
+
+static inline int do_ramdump(void *handle, struct ramdump_segment *segments,
+		int nsegments)
+{
+	return -ENODEV;
+}
+
+static inline int do_elf_ramdump(void *handle, struct ramdump_segment *segments,
+		int nsegments)
+{
+	return -ENODEV;
+}
+#endif /* CONFIG_MSM_SUBSYSTEM_RESTART */
+
+#endif
-- 
1.9.1

