#
# To generate platform/cld driver projects, and compile them to get KO
#

# Generate platform project
cd ./platform_patches
git init
git am 0001-platform-initial-point.patch
git am 0002-platform-Modify-Makefile-for-x86-platform.patch
git am 0003-platform-porting-inc-ipc_router-xprt-folder-modules-.patch
git am 0004-qmi-porting-qmi-module-to-Ubuntu-x86.patch
git am 0005-diag-porting-diag-module-to-Ubuntu-x86.patch
git am 0006-diag-create-netlink-in-diag-module-to-delever-log-to.patch
git am 0007-mhi-porting-mhi-module-to-Ubuntu-x86.patch
git am 0008-mhi-add-SoC-reset-method-for-Ubuntu-x86.patch
git am 0009-mhi-porting-fix-from-MSM-to-fix-FW-dump-failure-issu.patch
git am 0010-mhi-Add-FW-dump-support-on-Ubuntu-x86.patch
git am 0011-cnss2-porting-cnss2-module-to-Ubuntu-x86.patch
git am 0012-cnss2-Support-unified-QMI-sequence.patch
git am 0013-cnss2-Add-dump-file-support-on-Ubuntu-x86.patch
git am 0014-cnss2-Add-SoC-reset-support-on-Ubuntu-x86.patch
# Compile platform

make CONFIG_CNSS_QCA6390=y -j8

# Patch cld Makefile
cd ..
cd ./cld/qcacld-3.0
patch -p1 < ../../msm_patches/Patch-Makefile.patch
make CONFIG_CNSS2=y CONFIG_CNSS_QCA6390=y CONFIG_SLUB_DEBUG_ON=n CONFIG_SLUB_DEBUG=n CONFIG_LINUX_QCMBR=y DEVELOPER_DISABLE_BUILD_TIMESTAMP=y -j8
