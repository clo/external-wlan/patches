From 716064d26fa4725726924e0d8d20a982e211641e Mon Sep 17 00:00:00 2001
From: Jouni Malinen <j@w1.fi>
Date: Thu, 19 Oct 2017 12:41:43 +0800
Subject: [PATCH] Fix PTK rekeying to generate a new ANonce

The Authenticator state machine path for PTK rekeying ended up bypassing
the AUTHENTICATION2 state where a new ANonce is generated when going
directly to the PTKSTART state since there is no need to try to
determine the PMK again in such a case. This is far from ideal since the
new PTK would depend on a new nonce only from the supplicant.

Fix this by generating a new ANonce when moving to the PTKSTART state
for the purpose of starting new 4-way handshake to rekey PTK.

Signed-off-by: Jouni Malinen <j@w1.fi>

Git-commit: 0adc9b28b39d414d5febfff752f6a1576f785c85
Git-repo: git://w1.fi/srv/git/hostap.git
Change-Id: I962053dae6fc61fef69c5ab76c550eae3181d938
CRs-Fixed: 2128047
---
 src/ap/wpa_auth.c | 24 +++++++++++++++++++++---
 1 file changed, 21 insertions(+), 3 deletions(-)

diff --git a/src/ap/wpa_auth.c b/src/ap/wpa_auth.c
index 0286c5b..5a6122a 100644
--- a/src/ap/wpa_auth.c
+++ b/src/ap/wpa_auth.c
@@ -1641,6 +1641,21 @@ SM_STATE(WPA_PTK, AUTHENTICATION2)
 }
 
 
+static int wpa_auth_sm_ptk_update(struct wpa_state_machine *sm)
+{
+	if (random_get_bytes(sm->ANonce, WPA_NONCE_LEN)) {
+		wpa_printf(MSG_ERROR,
+			   "WPA: Failed to get random data for ANonce");
+		sm->Disconnect = TRUE;
+		return -1;
+	}
+	wpa_hexdump(MSG_DEBUG, "WPA: Assign new ANonce", sm->ANonce,
+		    WPA_NONCE_LEN);
+	sm->TimeoutCtr = 0;
+	return 0;
+}
+
+
 SM_STATE(WPA_PTK, INITPMK)
 {
 	u8 msk[2 * PMK_LEN];
@@ -2130,9 +2145,12 @@ SM_STEP(WPA_PTK)
 		SM_ENTER(WPA_PTK, AUTHENTICATION);
 	else if (sm->ReAuthenticationRequest)
 		SM_ENTER(WPA_PTK, AUTHENTICATION2);
-	else if (sm->PTKRequest)
-		SM_ENTER(WPA_PTK, PTKSTART);
-	else switch (sm->wpa_ptk_state) {
+	else if (sm->PTKRequest) {
+		if (wpa_auth_sm_ptk_update(sm) < 0)
+			SM_ENTER(WPA_PTK, DISCONNECTED);
+		else
+			SM_ENTER(WPA_PTK, PTKSTART);
+	} else switch (sm->wpa_ptk_state) {
 	case WPA_PTK_INITIALIZE:
 		break;
 	case WPA_PTK_DISCONNECT:
-- 
1.8.2.1
