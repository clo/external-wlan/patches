From 2726fd355e58a938e559c05bce9dfff682f5fb29 Mon Sep 17 00:00:00 2001
From: Mathy Vanhoef <Mathy.Vanhoef@cs.kuleuven.be>
Date: Thu, 19 Oct 2017 12:37:55 +0800
Subject: [PATCH] Prevent installation of an all-zero TK

Properly track whether a PTK has already been installed to the driver
and the TK part cleared from memory. This prevents an attacker from
trying to trick the client into installing an all-zero TK.

This fixes the earlier fix in commit
ad00d64e7d8827b3cebd665a0ceb08adabf15e1e ('Fix TK configuration to the
driver in EAPOL-Key 3/4 retry case') which did not take into account
possibility of an extra message 1/4 showing up between retries of
message 3/4.

Signed-off-by: Mathy Vanhoef <Mathy.Vanhoef@cs.kuleuven.be>

Git-commit: 53bb18cc8b7a4da72e47e4b3752d0d2135cffb23
Git-repo: git://w1.fi/srv/git/hostap.git
Change-Id: I59bf4dd012b40e38b2f874f2bcb6a3416fcb35d8
CRs-Fixed: 2112865
Conflicts:
	src/common/wpa_common.h
	src/rsn_supp/wpa.c
	src/rsn_supp/wpa_i.h
[Backport : This commit backports the current commit to address
 the above conflicts.
 src/common/wpa_common.h : add definition WPA_TK_MAX_LEN
 src/rsn_supp/wpa.c : clear ptk.tk1 per definition
 src/rsn_supp/wpa_i.h : changes not need as tk_to_set is not exist]
---
 src/common/wpa_common.h |  3 +++
 src/rsn_supp/wpa.c      | 10 ++++++++++
 2 files changed, 13 insertions(+)

diff --git a/src/common/wpa_common.h b/src/common/wpa_common.h
index 2d63662..8ca716e 100644
--- a/src/common/wpa_common.h
+++ b/src/common/wpa_common.h
@@ -176,6 +176,8 @@ struct wpa_eapol_key {
 	/* followed by key_data_length bytes of key_data */
 } STRUCT_PACKED;
 
+#define WPA_TK_MAX_LEN 32
+
 /**
  * struct wpa_ptk - WPA Pairwise Transient Key
  * IEEE Std 802.11i-2004 - 8.5.1.2 Pairwise key hierarchy
@@ -191,6 +193,7 @@ struct wpa_ptk {
 			u8 rx_mic_key[8];
 		} auth;
 	} u;
+	int installed; /* 1 if key has already been installed to driver */
 } STRUCT_PACKED;
 
 
diff --git a/src/rsn_supp/wpa.c b/src/rsn_supp/wpa.c
index 292255c..66f5f4a 100644
--- a/src/rsn_supp/wpa.c
+++ b/src/rsn_supp/wpa.c
@@ -515,6 +515,12 @@ static int wpa_supplicant_install_ptk(struct wpa_sm *sm,
 	const u8 *key_rsc;
 	u8 null_rsc[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
 
+	if (sm->ptk.installed) {
+		wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
+			"WPA: Do not re-install same PTK to the driver");
+		return 0;
+	}
+
 	wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
 		"WPA: Installing PTK to the driver");
 
@@ -551,6 +557,10 @@ static int wpa_supplicant_install_ptk(struct wpa_sm *sm,
 		return -1;
 	}
 
+	/* TK is not needed anymore in supplicant */
+	os_memset(sm->ptk.tk1, 0, WPA_TK_MAX_LEN);
+	sm->ptk.installed = 1;
+
 	if (sm->wpa_ptk_rekey) {
 		eloop_cancel_timeout(wpa_sm_rekey_ptk, sm, NULL);
 		eloop_register_timeout(sm->wpa_ptk_rekey, 0, wpa_sm_rekey_ptk,
-- 
1.8.2.1
