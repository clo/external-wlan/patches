From 8cf11123b51adfad96b73e38a9063cd5fe5b339c Mon Sep 17 00:00:00 2001
From: nakul kachhwaha <nkachhwa@codeaurora.org>
Date: Wed, 23 Jun 2021 15:46:41 +0530
Subject: [PATCH] net/bridge: Patching linux bridge code to support hyfi bridge

Signed-off-by: nakul kachhwaha <nkachhwa@codeaurora.org>
---
 include/linux/if_bridge.h | 22 ++++++++++++++++++++++
 net/bridge/br.c           |  4 ++++
 net/bridge/br_device.c    |  4 ++++
 net/bridge/br_fdb.c       |  2 ++
 net/bridge/br_if.c        |  4 ++++
 net/bridge/br_input.c     | 26 +++++++++++++++++++++++++-
 net/bridge/br_netlink.c   |  1 +
 net/bridge/br_private.h   | 12 ++++++++++++
 8 files changed, 74 insertions(+), 1 deletion(-)

diff --git a/include/linux/if_bridge.h b/include/linux/if_bridge.h
index c6587c0..e2ea917 100644
--- a/include/linux/if_bridge.h
+++ b/include/linux/if_bridge.h
@@ -17,6 +17,8 @@
 #include <uapi/linux/if_bridge.h>
 #include <linux/bitops.h>
 
+struct net_bridge_port;
+
 struct br_ip {
 	union {
 		__be32	ip4;
@@ -75,6 +77,26 @@ static inline bool br_multicast_has_querier_adjacent(struct net_device *dev,
 {
 	return false;
 }
+
+
 #endif
 
+typedef struct net_bridge_port *br_port_dev_get_hook_t(struct net_device *dev,
+                                                       struct sk_buff *skb,
+                                                       unsigned char *addr,
+                                                       unsigned int cookie);
+extern br_port_dev_get_hook_t __rcu *br_port_dev_get_hook;
+
+typedef struct net_bridge_port *br_get_dst_hook_t(const struct net_bridge_port *src,
+               struct sk_buff **skb);
+extern br_get_dst_hook_t __rcu *br_get_dst_hook;
+
+typedef int (br_multicast_handle_hook_t)(const struct net_bridge_port *src,
+               struct sk_buff *skb);
+extern br_multicast_handle_hook_t __rcu *br_multicast_handle_hook;
+
+typedef void (br_notify_hook_t)(int group, int event, const void *ptr);
+extern br_notify_hook_t __rcu *br_notify_hook;
+
+
 #endif
diff --git a/net/bridge/br.c b/net/bridge/br.c
index 889e564..389f1aa 100644
--- a/net/bridge/br.c
+++ b/net/bridge/br.c
@@ -268,6 +268,10 @@ static void __exit br_deinit(void)
 	br_fdb_fini();
 }
 
+/* Hook for bridge event notifications */
+br_notify_hook_t __rcu *br_notify_hook __read_mostly;
+EXPORT_SYMBOL_GPL(br_notify_hook);
+
 module_init(br_init)
 module_exit(br_deinit)
 MODULE_LICENSE("GPL");
diff --git a/net/bridge/br_device.c b/net/bridge/br_device.c
index 89a687f..1de63bf 100644
--- a/net/bridge/br_device.c
+++ b/net/bridge/br_device.c
@@ -64,6 +64,10 @@ netdev_tx_t br_dev_xmit(struct sk_buff *skb, struct net_device *dev)
 	if (is_broadcast_ether_addr(dest)) {
 		br_flood(br, skb, BR_PKT_BROADCAST, false, true);
 	} else if (is_multicast_ether_addr(dest)) {
+		br_multicast_handle_hook_t *multicast_handle_hook = rcu_dereference(br_multicast_handle_hook);
+		if (!__br_get(multicast_handle_hook, true, NULL, skb))
+			goto out;
+
 		if (unlikely(netpoll_tx_running(dev))) {
 			br_flood(br, skb, BR_PKT_MULTICAST, false, true);
 			goto out;
diff --git a/net/bridge/br_fdb.c b/net/bridge/br_fdb.c
index 6b43c8c..73dd1fe 100644
--- a/net/bridge/br_fdb.c
+++ b/net/bridge/br_fdb.c
@@ -390,6 +390,7 @@ struct net_bridge_fdb_entry *__br_fdb_get(struct net_bridge *br,
 
 	return NULL;
 }
+EXPORT_SYMBOL_GPL(__br_fdb_get);
 
 #if IS_ENABLED(CONFIG_ATM_LANE)
 /* Interface used by ATM LANE hook to test
@@ -699,6 +700,7 @@ static void fdb_notify(struct net_bridge *br,
 		kfree_skb(skb);
 		goto errout;
 	}
+	__br_notify(RTNLGRP_NEIGH, type, fdb);
 	rtnl_notify(skb, net, 0, RTNLGRP_NEIGH, NULL, GFP_ATOMIC);
 	return;
 errout:
diff --git a/net/bridge/br_if.c b/net/bridge/br_if.c
index ed0dd33..2dab94f 100644
--- a/net/bridge/br_if.c
+++ b/net/bridge/br_if.c
@@ -28,6 +28,10 @@
 
 #include "br_private.h"
 
+/* Hook for external forwarding logic */
+br_port_dev_get_hook_t __rcu *br_port_dev_get_hook __read_mostly;
+EXPORT_SYMBOL_GPL(br_port_dev_get_hook);
+
 /*
  * Determine initial path cost based on speed.
  * using recommendations from 802.1d standard
diff --git a/net/bridge/br_input.c b/net/bridge/br_input.c
index 855b72f..e82a91d 100644
--- a/net/bridge/br_input.c
+++ b/net/bridge/br_input.c
@@ -26,6 +26,14 @@
 br_should_route_hook_t __rcu *br_should_route_hook __read_mostly;
 EXPORT_SYMBOL(br_should_route_hook);
 
+/* Hook for external Multicast handler */
+br_multicast_handle_hook_t __rcu *br_multicast_handle_hook __read_mostly;
+EXPORT_SYMBOL_GPL(br_multicast_handle_hook);
+
+/* Hook for external forwarding logic */
+br_get_dst_hook_t __rcu *br_get_dst_hook __read_mostly;
+EXPORT_SYMBOL_GPL(br_get_dst_hook);
+
 static int
 br_netif_receive_skb(struct net *net, struct sock *sk, struct sk_buff *skb)
 {
@@ -136,6 +144,8 @@ int br_handle_frame_finish(struct net *net, struct sock *sk, struct sk_buff *skb
 	bool local_rcv, mcast_hit = false;
 	struct net_bridge *br;
 	u16 vid = 0;
+	struct net_bridge_port *pdst = NULL;
+	br_get_dst_hook_t *get_dst_hook = rcu_dereference(br_get_dst_hook);
 
 	if (!p || p->state == BR_STATE_DISABLED)
 		goto drop;
@@ -173,6 +183,12 @@ int br_handle_frame_finish(struct net *net, struct sock *sk, struct sk_buff *skb
 
 	switch (pkt_type) {
 	case BR_PKT_MULTICAST:
+		if (!is_broadcast_ether_addr(dest)) {
+			br_multicast_handle_hook_t *multicast_handle_hook = rcu_dereference(br_multicast_handle_hook);
+			if (!__br_get(multicast_handle_hook, true, p, skb))
+				goto out;
+		}
+
 		mdst = br_mdb_get(br, skb, vid);
 		if ((mdst || BR_INPUT_SKB_CB_MROUTERS_ONLY(skb)) &&
 		    br_multicast_querier_exists(br, eth_hdr(skb))) {
@@ -188,7 +204,13 @@ int br_handle_frame_finish(struct net *net, struct sock *sk, struct sk_buff *skb
 		}
 		break;
 	case BR_PKT_UNICAST:
-		dst = __br_fdb_get(br, dest, vid);
+		pdst = __br_get(get_dst_hook, NULL, p, &skb);
+                if (pdst) {
+                        if (!skb)
+                                goto out;
+                } else {
+			dst = __br_fdb_get(br, dest, vid);
+		}
 	default:
 		break;
 	}
@@ -199,6 +221,8 @@ int br_handle_frame_finish(struct net *net, struct sock *sk, struct sk_buff *skb
 
 		dst->used = jiffies;
 		br_forward(dst->dst, skb, local_rcv, false);
+	} else if (pdst) {
+		br_forward(pdst, skb, local_rcv, false);
 	} else {
 		if (!mcast_hit)
 			br_flood(br, skb, pkt_type, local_rcv, false);
diff --git a/net/bridge/br_netlink.c b/net/bridge/br_netlink.c
index 0474106..af90213 100644
--- a/net/bridge/br_netlink.c
+++ b/net/bridge/br_netlink.c
@@ -457,6 +457,7 @@ void br_ifinfo_notify(int event, struct net_bridge_port *port)
 		kfree_skb(skb);
 		goto errout;
 	}
+	__br_notify(RTNLGRP_LINK, event, port);
 	rtnl_notify(skb, net, 0, RTNLGRP_LINK, NULL, GFP_ATOMIC);
 	return;
 errout:
diff --git a/net/bridge/br_private.h b/net/bridge/br_private.h
index 1b63177..9eec9d6 100644
--- a/net/bridge/br_private.h
+++ b/net/bridge/br_private.h
@@ -21,6 +21,7 @@
 #include <net/ip6_fib.h>
 #include <linux/if_vlan.h>
 #include <linux/rhashtable.h>
+#include <linux/export.h>
 
 #define BR_HASH_BITS 8
 #define BR_HASH_SIZE (1 << BR_HASH_BITS)
@@ -1076,4 +1077,15 @@ static inline bool nbp_switchdev_allowed_egress(const struct net_bridge_port *p,
 }
 #endif /* CONFIG_NET_SWITCHDEV */
 
+#define __br_get( __hook, __default, __args ... ) \
+               (__hook ? (__hook( __args )) : (__default))
+
+static inline void __br_notify(int group, int type, const void *data)
+{
+       br_notify_hook_t *notify_hook = rcu_dereference(br_notify_hook);
+
+       if (notify_hook)
+               notify_hook(group, type, data);
+}
+
 #endif
-- 
2.7.4

