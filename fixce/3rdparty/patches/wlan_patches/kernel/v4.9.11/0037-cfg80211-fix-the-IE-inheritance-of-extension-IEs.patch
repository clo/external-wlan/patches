From ac49df867f6a2a591a871d705c1f284e1d4e21a1 Mon Sep 17 00:00:00 2001
From: Sara Sharon <sara.sharon@intel.com>
Date: Tue, 29 Jan 2019 14:00:58 +0200
Subject: [PATCH 53/57] cfg80211: fix the IE inheritance of extension IEs

Extension IEs have ID 255 followed by extension ID. Current
code is buggy in handling it in two ways:
1. When checking if IE is in the frame, it uses just the ID, which
   for extension elements is too broad.
2. It uses 0xFF to mark copied IEs, which will result in not copying
   extension IEs from the subelement.

Fix both issue.

Signed-off-by: Sara Sharon <sara.sharon@intel.com>
Signed-off-by: Johannes Berg <johannes.berg@intel.com>
Change-Id: Ie13620a9e6c9bdde23f3de67426abf123a67b6e1
CRs-fixed: 2399972
Git-commit: c17fe043a3b79255c6cbe76aafb594849fac0005
Git-repo: git://git.kernel.org/pub/scm/linux/kernel/git/jberg/mac80211-next.git
Signed-off-by: Peng Xu <pxu@codeaurora.org>
Signed-off-by: Liangwei Dong <liangwei@codeaurora.org>
---
 net/wireless/scan.c | 20 +++++++++++++-------
 1 file changed, 13 insertions(+), 7 deletions(-)

diff --git a/net/wireless/scan.c b/net/wireless/scan.c
index 3d38f2f..7237117 100644
--- a/net/wireless/scan.c
+++ b/net/wireless/scan.c
@@ -213,7 +213,13 @@ static size_t cfg80211_gen_new_ie(const u8 *ie, size_t ielen,
 			continue;
 		}
 
-		tmp = (u8 *)cfg80211_find_ie(tmp_old[0], sub_copy, subie_len);
+		if (tmp_old[0] == WLAN_EID_EXTENSION)
+			tmp = (u8 *)cfg80211_find_ext_ie(tmp_old[2], sub_copy,
+							 subie_len);
+		else
+			tmp = (u8 *)cfg80211_find_ie(tmp_old[0], sub_copy,
+						     subie_len);
+
 		if (!tmp) {
 			/* ie in old ie but not in subelement */
 			if (tmp_old[0] != WLAN_EID_MULTIPLE_BSSID) {
@@ -223,8 +229,9 @@ static size_t cfg80211_gen_new_ie(const u8 *ie, size_t ielen,
 		} else {
 			/* ie in transmitting ie also in subelement,
 			 * copy from subelement and flag the ie in subelement
-			 * as copied (by setting eid field to 0xff). For
-			 * vendor ie, compare OUI + type + subType to
+			 * as copied (by setting eid field to WLAN_EID_SSID,
+			 * which is skipped anyway).
+			 * For vendor ie, compare OUI + type + subType to
 			 * determine if they are the same ie.
 			 */
 			if (tmp_old[0] == WLAN_EID_VENDOR_SPECIFIC) {
@@ -234,7 +241,7 @@ static size_t cfg80211_gen_new_ie(const u8 *ie, size_t ielen,
 					 */
 					memcpy(pos, tmp, tmp[1] + 2);
 					pos += tmp[1] + 2;
-					tmp[0] = 0xff;
+					tmp[0] = WLAN_EID_SSID;
 				} else {
 					memcpy(pos, tmp_old, tmp_old[1] + 2);
 					pos += tmp_old[1] + 2;
@@ -243,7 +250,7 @@ static size_t cfg80211_gen_new_ie(const u8 *ie, size_t ielen,
 				/* copy ie from subelement into new ie */
 				memcpy(pos, tmp, tmp[1] + 2);
 				pos += tmp[1] + 2;
-				tmp[0] = 0xff;
+				tmp[0] = WLAN_EID_SSID;
 			}
 		}
 
@@ -260,8 +267,7 @@ static size_t cfg80211_gen_new_ie(const u8 *ie, size_t ielen,
 	while (tmp_new + tmp_new[1] + 2 - sub_copy <= subie_len) {
 		if (!(tmp_new[0] == WLAN_EID_NON_TX_BSSID_CAP ||
 		      tmp_new[0] == WLAN_EID_SSID ||
-		      tmp_new[0] == WLAN_EID_MULTI_BSSID_IDX ||
-		      tmp_new[0] == 0xff)) {
+		      tmp_new[0] == WLAN_EID_MULTI_BSSID_IDX)) {
 			memcpy(pos, tmp_new, tmp_new[1] + 2);
 			pos += tmp_new[1] + 2;
 		}
-- 
1.9.1

