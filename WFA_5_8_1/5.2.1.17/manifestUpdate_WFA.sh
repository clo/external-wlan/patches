#!/bin/bash
#
# SCRIPT TO OVERRIDE PROJECTS IN A GIVEN MANIFEST .XML FILE
#
# Contact: Pdhavali@codeaurora.org
#
# $Id: //depot/software/swbuild/bin/la_integ/utils/WFA/OpenQ835/manifestUpdate_WFA.sh#1 $
# $DateTime: 2018/02/12 12:00:00 $
# $Author: pdhavali $
# $modified: mkoyyala $
#
# BUILD13_EXTERNAL

input_file=$1
output_file="${input_file}.out"

# Array to hold projects that need to be replaced or added to defaut manifest.xml
declare -A replace

# Replace manifest entries for these projects
# replace["kernel/msm-4.4"]='<project name="kernel/msm-4.4" revision="1f3c49d418238e20a9eb256920b8f6fb149643bb" upstream="refs/heads/kernel.lnx.4.4-r15-rel"/>'

replace["platform/vendor/qcom-opensource/wlan/utils/sigma-dut"]='<project name="qca/sigma-dut" remote="github" path="vendor/qcom/opensource/wlan/utils/sigma-dut" revision="2508a59d9b8a6402a905973ca2efcdf5e3adeec6" upstream="master"/>'


tmpfile=$(mktemp /tmp/prjtmpXXXX)
rm -fv $output_file

while IFS= read -r line
do

	# Skip </manifest> until all lines are spit out
	if echo "$line" | egrep -q "</manifest>";
	then
		continue
	fi

	# Process only <project line and for others show them as-is
	if echo "$line" | egrep -qv "<project ";
	then
		echo "$line" >> $output_file
		continue
	fi

	linetmp=$(echo "$line" | sed -e 's/"//g')

	echo "$linetmp" | fmt -1 | grep name= | awk -F= '{print $2}' > $tmpfile
	project=$(cat $tmpfile | col -b)

	# If project needs to be replacen, skip it here
	if [ "${replace[$project]}" == "" ]; then
		echo "$line" >> $output_file
	fi
done < "$input_file"
rm -f $tmpfile

# Name=caf pointing to https://source.codeaurora.org/quic/la/ already exists in $input_file
echo ' <!-- START: Qualcomm additional project mappings -->'             >> $output_file
echo ' <remote fetch="git://git-android.quicinc.com/" name="qcremote"/>' >> $output_file
echo ' <remote fetch="git://github.com/" name="github"/>'                >> $output_file
echo ' <remote fetch="git://w1.fi/" name="supp" />'                      >> $output_file
echo ' <project name="esnet/iperf" remote="github" path="external/iperf" revision="af34c411dfbb2a452543311f1989f445edb3b3c1" upstream="master"/>'      >> $output_file
echo ' <project name="platform/vendor/qcom-opensource/wlan/apps" path="vendor/qcom/opensource/wlan/apps" revision="12f74f46e6ce39f4c5c7d4584c4400e4b73712d5" upstream="refs/heads/wlan-service.lnx.1.9.r7-rel"/>'      >> $output_file

# Finally append the new projects that are not in CAF
for prj in ${!replace[@]}
do
	echo "  ${replace[$prj]}"                            >> $output_file
done
echo ' <!-- END  : Qualcomm additional project mappings -->' >> $output_file
echo "</manifest>"                                           >> $output_file

