======================================================
Instructions to apply patches given in this directory:
======================================================
After following instructions as per distributed 5.2.1.14
Release Notes perform following additional steps:

1) Apply patches contained in wlan_patches/xxxxx folder
   in respective git project paths.
======================================================

