From f3a9c1cbd441e478aee67babfcdebfea3c947487 Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Wed, 8 Jul 2020 14:15:45 +0800
Subject: [PATCH 134/134] ath11k: put ath11k fw files to ath11k related
 directory

With this change, QCA6390 fw related files are put in
/lib/firmware/ath11k/QCA6390/hw2.0/ instead of /lib/firmware/

Tested-on: QCA6390 hw2.0 PCI WLAN.HST.1.0.1-01736-QCAHSTSWPLZ_V2_TO_X86-1

Change-Id: I1c56d387258b48282fb70b2dc49ab20b23499835
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/core.c | 6 +++++-
 drivers/net/wireless/ath/ath11k/core.h | 8 ++++++++
 drivers/net/wireless/ath/ath11k/mhi.c  | 6 +++++-
 drivers/net/wireless/ath/ath11k/pci.h  | 3 +++
 drivers/net/wireless/ath/ath11k/qmi.c  | 6 +++++-
 drivers/net/wireless/ath/ath11k/qmi.h  | 2 +-
 6 files changed, 27 insertions(+), 4 deletions(-)

diff --git a/drivers/net/wireless/ath/ath11k/core.c b/drivers/net/wireless/ath/ath11k/core.c
index c4a0436..647621d 100644
--- a/drivers/net/wireless/ath/ath11k/core.c
+++ b/drivers/net/wireless/ath/ath11k/core.c
@@ -42,6 +42,9 @@ static const struct ath11k_hw_params ath11k_hw_params_list[] = {
 	{
 		.name = "qca6390",
 		.dev_id = ATH11K_HW_QCA6390,
+		.fw = {
+			.dir = "QCA6390/hw2.0",
+		},
 		.internal_sleep_clock = true,
 		.single_pdev_only = true,
 		.hw_ops = &ath11k_hw_ops_qca6x90,
@@ -102,7 +105,8 @@ static const struct firmware *ath11k_fetch_fw_file(struct ath11k_base *ab,
 	if (dir == NULL)
 		dir = ".";
 
-	snprintf(filename, sizeof(filename), "%s/%s", dir, file);
+	ath11k_core_create_firmware_path(ab, file, filename, 100);
+
 	ret = firmware_request_nowarn(&fw, filename, ab->dev);
 	ath11k_dbg(ab, ATH11K_DBG_BOOT, "boot fw request '%s': %d\n",
 		   filename, ret);
diff --git a/drivers/net/wireless/ath/ath11k/core.h b/drivers/net/wireless/ath/ath11k/core.h
index 574867f..6a2733f 100644
--- a/drivers/net/wireless/ath/ath11k/core.h
+++ b/drivers/net/wireless/ath/ath11k/core.h
@@ -965,4 +965,12 @@ static inline const char *ath11k_bus_str(enum ath11k_bus bus)
 
 	return "unknown";
 }
+
+static inline void ath11k_core_create_firmware_path(struct ath11k_base *ab,
+						    const char *filename,
+						    void *buf, size_t buf_len)
+{
+	snprintf(buf, buf_len, "%s/%s/%s", ATH11K_FW_DIR,
+		 ab->hw_params.fw.dir, filename);
+}
 #endif /* _CORE_H_ */
diff --git a/drivers/net/wireless/ath/ath11k/mhi.c b/drivers/net/wireless/ath/ath11k/mhi.c
index 0aeee1a..c9fea35 100644
--- a/drivers/net/wireless/ath/ath11k/mhi.c
+++ b/drivers/net/wireless/ath/ath11k/mhi.c
@@ -256,7 +256,11 @@ int ath11k_pci_register_mhi(struct ath11k_pci *ab_pci)
 
 	ab_pci->mhi_ctrl = mhi_ctrl;
 	mhi_ctrl->cntrl_dev = &pci_dev->dev;
-	mhi_ctrl->fw_image = ATH11K_PCI_FW_FILE_NAME;
+	ath11k_core_create_firmware_path(ab, ATH11K_PCI_FW_FILE_NAME,
+					 ab_pci->fw_image,
+					 FW_IMAGE_NAME_MAX_LEN);
+	mhi_ctrl->fw_image = ab_pci->fw_image;
+
 	mhi_ctrl->regs = ab_pci->mem;
 
 	ret = ath11k_pci_get_mhi_msi(ab_pci);
diff --git a/drivers/net/wireless/ath/ath11k/pci.h b/drivers/net/wireless/ath/ath11k/pci.h
index e441fe4..561688f 100644
--- a/drivers/net/wireless/ath/ath11k/pci.h
+++ b/drivers/net/wireless/ath/ath11k/pci.h
@@ -75,6 +75,8 @@
 
 #define ATH11K_IRQ_CE0_OFFSET 3
 
+#define FW_IMAGE_NAME_MAX_LEN          100
+
 struct ath11k_msi_user {
 	char *name;
 	int num_vectors;
@@ -104,6 +106,7 @@ struct ath11k_pci {
 	bool init_done;
 	u8 aspm;
 	bool restore_aspm;
+	char fw_image[FW_IMAGE_NAME_MAX_LEN];
 };
 
 int ath11k_pci_get_user_msi_assignment(struct ath11k_pci *ar_pci, char *user_name,
diff --git a/drivers/net/wireless/ath/ath11k/qmi.c b/drivers/net/wireless/ath/ath11k/qmi.c
index 3c4c706..57f4d40 100644
--- a/drivers/net/wireless/ath/ath11k/qmi.c
+++ b/drivers/net/wireless/ath/ath11k/qmi.c
@@ -2102,7 +2102,11 @@ static int ath11k_load_m3_bin(struct ath11k_base *ab)
 	int ret;
 
 	if (!m3_mem->vaddr && !m3_mem->size) {
-		snprintf(filename, sizeof(filename), ATH11K_DEFAULT_M3_FILE_NAME);
+		ath11k_core_create_firmware_path(ab,
+						 ATH11K_DEFAULT_M3_FILE_NAME,
+						 filename,
+						 ATH11K_MAX_M3_FILE_NAME_LENGTH);
+
 		ret = request_firmware(&fw_entry, filename,
 				       ab->dev);
 		if (ret) {
diff --git a/drivers/net/wireless/ath/ath11k/qmi.h b/drivers/net/wireless/ath/ath11k/qmi.h
index c3d6a22..e85367f 100644
--- a/drivers/net/wireless/ath/ath11k/qmi.h
+++ b/drivers/net/wireless/ath/ath11k/qmi.h
@@ -35,7 +35,7 @@
 #define QMI_WLANFW_MAX_DATA_SIZE_V01		6144
 #define ATH11K_FIRMWARE_MODE_OFF		4
 #define ATH11K_QMI_TARGET_MEM_MODE_DEFAULT	0
-#define ATH11K_MAX_M3_FILE_NAME_LENGTH		13
+#define ATH11K_MAX_M3_FILE_NAME_LENGTH		100
 
 struct ath11k_base;
 
-- 
2.7.4

