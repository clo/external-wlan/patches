From 7cb73c6ad1af271f3ab9dba105a85cd3202a4ad9 Mon Sep 17 00:00:00 2001
From: Hemant Kumar <hemantk@codeaurora.org>
Date: Mon, 4 May 2020 19:24:18 -0700
Subject: [PATCH 019/124] bus: mhi: core: Add support for processing of
 sleepable events

Processing of some events requires to call sleeping function.
In order to handle those events work is queued using high
priority workqueue. Sleepable events are only supported in
mission mode using dedicated event ring.

Change-Id: Iccb77bc7f12603b057b5fee6cf1acfe55d9ebc87
Signed-off-by: Hemant Kumar <hemantk@codeaurora.org>
---
 drivers/bus/mhi/core/init.c     | 12 ++++++++++++
 drivers/bus/mhi/core/internal.h |  2 ++
 drivers/bus/mhi/core/main.c     | 21 +++++++++++++++++++++
 drivers/bus/mhi/core/pm.c       | 17 ++++++++++++-----
 include/linux/mhi.h             |  6 ++++++
 5 files changed, 53 insertions(+), 5 deletions(-)

diff --git a/drivers/bus/mhi/core/init.c b/drivers/bus/mhi/core/init.c
index 9a6e828..0cfb2dc 100644
--- a/drivers/bus/mhi/core/init.c
+++ b/drivers/bus/mhi/core/init.c
@@ -684,6 +684,10 @@ static int parse_ev_cfg(struct mhi_controller *mhi_cntrl,
 
 		mhi_event->cl_manage = event_cfg->client_managed;
 		mhi_event->offload_ev = event_cfg->offload_channel;
+
+		if (mhi_event->priority == MHI_ER_PRIORITY_HI_SLEEP)
+			INIT_WORK(&mhi_event->ev_work, mhi_process_ev_work);
+
 		mhi_event++;
 	}
 
@@ -891,6 +895,11 @@ int mhi_register_controller(struct mhi_controller *mhi_cntrl,
 	INIT_WORK(&mhi_cntrl->st_worker, mhi_pm_st_worker);
 	init_waitqueue_head(&mhi_cntrl->state_event);
 
+	mhi_cntrl->hi_ev_wq = alloc_ordered_workqueue
+				("mhi_ev_w", WQ_MEM_RECLAIM | WQ_HIGHPRI);
+	if (!mhi_cntrl->hi_ev_wq)
+		goto error_alloc_cmd;
+
 	mhi_cmd = mhi_cntrl->mhi_cmd;
 	for (i = 0; i < NR_OF_CMD_RINGS; i++, mhi_cmd++)
 		spin_lock_init(&mhi_cmd->lock);
@@ -978,6 +987,7 @@ int mhi_register_controller(struct mhi_controller *mhi_cntrl,
 
 error_alloc_dev:
 	kfree(mhi_cntrl->mhi_cmd);
+	destroy_workqueue(mhi_cntrl->hi_ev_wq);
 
 error_alloc_cmd:
 	vfree(mhi_cntrl->mhi_chan);
@@ -996,6 +1006,8 @@ void mhi_unregister_controller(struct mhi_controller *mhi_cntrl)
 	mhi_destroy_sysfs(mhi_cntrl);
 	mhi_destroy_debugfs(mhi_cntrl);
 
+	destroy_workqueue(mhi_cntrl->hi_ev_wq);
+
 	kfree(mhi_cntrl->mhi_cmd);
 	kfree(mhi_cntrl->mhi_event);
 
diff --git a/drivers/bus/mhi/core/internal.h b/drivers/bus/mhi/core/internal.h
index cc6fe79..0195bd6 100644
--- a/drivers/bus/mhi/core/internal.h
+++ b/drivers/bus/mhi/core/internal.h
@@ -527,6 +527,7 @@ struct mhi_event {
 	struct mhi_ring ring;
 	struct db_cfg db_cfg;
 	struct tasklet_struct task;
+	struct work_struct ev_work;
 	spinlock_t lock;
 	int (*process_event)(struct mhi_controller *mhi_cntrl,
 			     struct mhi_event *mhi_event,
@@ -697,6 +698,7 @@ int mhi_process_data_event_ring(struct mhi_controller *mhi_cntrl,
 				struct mhi_event *mhi_event, u32 event_quota);
 int mhi_process_ctrl_ev_ring(struct mhi_controller *mhi_cntrl,
 			     struct mhi_event *mhi_event, u32 event_quota);
+void mhi_process_ev_work(struct work_struct *work);
 
 /* ISR handlers */
 irqreturn_t mhi_irq_handler(int irq_number, void *dev);
diff --git a/drivers/bus/mhi/core/main.c b/drivers/bus/mhi/core/main.c
index 6d5305a..bb842e9 100644
--- a/drivers/bus/mhi/core/main.c
+++ b/drivers/bus/mhi/core/main.c
@@ -374,6 +374,9 @@ irqreturn_t mhi_irq_handler(int irq_number, void *priv)
 	case MHI_ER_PRIORITY_DEFAULT_NOSLEEP:
 		tasklet_schedule(&mhi_event->task);
 		break;
+	case MHI_ER_PRIORITY_HI_SLEEP:
+		queue_work(mhi_cntrl->hi_ev_wq, &mhi_event->ev_work);
+		break;
 	default:
 		dev_dbg(dev, "skip unknown priority event\n");
 		break;
@@ -964,6 +967,24 @@ void mhi_ctrl_ev_task(unsigned long data)
 	}
 }
 
+void mhi_process_ev_work(struct work_struct *work)
+{
+	struct mhi_event *mhi_event = container_of(work, struct mhi_event,
+							ev_work);
+	struct mhi_controller *mhi_cntrl = mhi_event->mhi_cntrl;
+	struct device *dev = mhi_cntrl->cntrl_dev;
+
+	dev_dbg(dev, "Enter with pm_state:%s MHI_STATE:%s ee:%s\n",
+		to_mhi_pm_state_str(mhi_cntrl->pm_state),
+		TO_MHI_STATE_STR(mhi_cntrl->dev_state),
+		TO_MHI_EXEC_STR(mhi_cntrl->ee));
+
+	if (unlikely(MHI_EVENT_ACCESS_INVALID(mhi_cntrl->pm_state)))
+		return;
+
+	mhi_event->process_event(mhi_cntrl, mhi_event, U32_MAX);
+}
+
 static bool mhi_is_ring_full(struct mhi_controller *mhi_cntrl,
 			     struct mhi_ring *ring)
 {
diff --git a/drivers/bus/mhi/core/pm.c b/drivers/bus/mhi/core/pm.c
index 082bb5d..9fe55c0 100644
--- a/drivers/bus/mhi/core/pm.c
+++ b/drivers/bus/mhi/core/pm.c
@@ -155,20 +155,21 @@ static void mhi_setup_event_rings(struct mhi_controller *mhi_cntrl, bool add_el)
 {
 	struct mhi_event *mhi_event;
 	int i;
-	bool skip_er_setup;
+	bool skip_er_setup, sleep_ev;
 
 	mhi_event = mhi_cntrl->mhi_event;
 	for (i = 0; i < mhi_cntrl->total_ev_rings; i++, mhi_event++) {
 		struct mhi_ring *ring = &mhi_event->ring;
+		sleep_ev = (mhi_event->priority == MHI_ER_PRIORITY_HI_SLEEP);
 
 		if (mhi_event->offload_ev)
 			continue;
 
-		/* skip HW event ring setup in ready state */
+		/* skip HW and sleepable event ring setup in ready state */
 		if (mhi_cntrl->dev_state == MHI_STATE_READY)
-			skip_er_setup = mhi_event->hw_ring;
+			skip_er_setup = mhi_event->hw_ring || sleep_ev;
 		else
-			skip_er_setup = !mhi_event->hw_ring;
+			skip_er_setup = !mhi_event->hw_ring && !sleep_ev;
 
 		/* if no er element to add, ring all er dbs */
 		if (add_el && skip_er_setup)
@@ -430,7 +431,10 @@ static int mhi_pm_mission_mode_transition(struct mhi_controller *mhi_cntrl)
 		goto error_mission_mode;
 	}
 
-	/* Add elements to all HW event rings and ring HW event ring dbs */
+	/*
+	 * Add elements to all HW/sleep event rings and
+	 * ring HW/sleep event ring dbs
+	 */
 	mhi_setup_event_rings(mhi_cntrl, true);
 
 	read_unlock_bh(&mhi_cntrl->pm_lock);
@@ -566,6 +570,9 @@ static void mhi_pm_disable_transition(struct mhi_controller *mhi_cntrl,
 		if (mhi_event->offload_ev)
 			continue;
 
+		if (mhi_event->priority == MHI_ER_PRIORITY_HI_SLEEP)
+			cancel_work_sync(&mhi_event->ev_work);
+
 		ring->rp = ring->base;
 		ring->wp = ring->base;
 		er_ctxt->rp = er_ctxt->rbase;
diff --git a/include/linux/mhi.h b/include/linux/mhi.h
index 9ac7f93..5f2e353 100644
--- a/include/linux/mhi.h
+++ b/include/linux/mhi.h
@@ -195,10 +195,12 @@ enum mhi_er_data_type {
  * enum mhi_er_priority - Event ring processing priority
  * @MHI_ER_PRIORITY_DEFAULT_NOSLEEP: processed by tasklet
  * @MHI_ER_PRIORITY_HI_NOSLEEP: processed by hi-priority tasklet
+ * @MHI_ER_PRIORITY_HI_SLEEP: processed by hi-priority wq
  */
 enum mhi_er_priority {
 	MHI_ER_PRIORITY_DEFAULT_NOSLEEP,
 	MHI_ER_PRIORITY_HI_NOSLEEP,
+	MHI_ER_PRIORITY_HI_SLEEP,
 };
 
 /**
@@ -444,6 +446,10 @@ struct mhi_controller {
 	spinlock_t wlock;
 	struct mhi_link_info mhi_link_info;
 	struct work_struct st_worker;
+
+	/* workqueue to process sleepable events */
+	struct workqueue_struct *hi_ev_wq;
+
 	wait_queue_head_t state_event;
 
 	void (*status_cb)(struct mhi_controller *mhi_cntrl,
-- 
2.7.4

