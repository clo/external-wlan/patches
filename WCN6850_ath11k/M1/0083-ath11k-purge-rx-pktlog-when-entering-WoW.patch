From 21d9a2613e5eb2d1773e76cc7ffbffa714c9760a Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Mon, 16 Mar 2020 10:11:24 +0800
Subject: [PATCH 083/124] ath11k: purge rx pktlog when entering WoW

This change is to purge rx pktlog when entering WoW and reap
the mon_status buffer to keep it empty. When leaving WoW, host
restarts the reap timer. In WoW state, it's not allowed to feed
into mon_status rings.

Tested-on: QCA6390 WLAN.HST.1.0.1-01230-QCAHSTSWPLZ_V2_TO_X86-1

Change-Id: Iea980a075e52c768e5b9c2b1d110c50fbb4f9767
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/dp.h    |  2 ++
 drivers/net/wireless/ath/ath11k/dp_rx.c | 26 +++++++++++++++++++++++++-
 drivers/net/wireless/ath/ath11k/dp_rx.h |  2 +-
 drivers/net/wireless/ath/ath11k/dp_tx.c |  1 +
 drivers/net/wireless/ath/ath11k/mac.c   | 32 ++++++++++++++++++++++++++++++++
 drivers/net/wireless/ath/ath11k/mac.h   |  2 ++
 drivers/net/wireless/ath/ath11k/wow.c   | 21 +++++++++++++++++++++
 7 files changed, 84 insertions(+), 2 deletions(-)

diff --git a/drivers/net/wireless/ath/ath11k/dp.h b/drivers/net/wireless/ath/ath11k/dp.h
index 3881483..74ba1d6 100644
--- a/drivers/net/wireless/ath/ath11k/dp.h
+++ b/drivers/net/wireless/ath/ath11k/dp.h
@@ -43,6 +43,8 @@ struct dp_rx_tid {
 
 #define DP_REO_DESC_FREE_THRESHOLD  64
 #define DP_REO_DESC_FREE_TIMEOUT_MS 1000
+#define DP_MON_PURGE_TIMEOUT_MS     100
+#define DP_MON_SERVICE_BUDGET       128
 
 struct dp_reo_cache_flush_elem {
 	struct list_head list;
diff --git a/drivers/net/wireless/ath/ath11k/dp_rx.c b/drivers/net/wireless/ath/ath11k/dp_rx.c
index 463608a..f2af47a 100644
--- a/drivers/net/wireless/ath/ath11k/dp_rx.c
+++ b/drivers/net/wireless/ath/ath11k/dp_rx.c
@@ -266,11 +266,35 @@ void ath11k_dp_service_mon_ring(struct timer_list *t)
 	int i;
 
 	for (i = 0; i < NUM_RXDMA_PER_PDEV; i++)
-		ath11k_dp_rx_process_mon_rings(ab, i, NULL, 0x80);
+		ath11k_dp_rx_process_mon_rings(ab, i, NULL, DP_MON_SERVICE_BUDGET);
 
 	mod_timer(&ab->mon_reap_timer, jiffies +
 		  msecs_to_jiffies(ATH11K_MON_TIMER_INTERVAL));
 }
+
+int ath11k_dp_purge_mon_ring(struct ath11k_base *ab)
+{
+	int i, buf_reaped = 0;
+	unsigned long ts = jiffies;
+
+again:
+	for (i = 0; i < NUM_RXDMA_PER_PDEV; i++) {
+		buf_reaped += ath11k_dp_rx_process_mon_rings(ab, i,
+							     NULL,
+							     DP_MON_SERVICE_BUDGET);
+	}
+
+	/* nothing more to reap */
+	if (buf_reaped < DP_MON_SERVICE_BUDGET)
+		return 0;
+
+	if (time_after(jiffies, ts +
+		       msecs_to_jiffies(DP_MON_PURGE_TIMEOUT_MS)))
+		return -ETIMEDOUT;
+
+	goto again;
+}
+
 /* Returns number of Rx buffers replenished */
 int ath11k_dp_rxbufs_replenish(struct ath11k_base *ab, int mac_id,
 			       struct dp_rxdma_ring *rx_ring,
diff --git a/drivers/net/wireless/ath/ath11k/dp_rx.h b/drivers/net/wireless/ath/ath11k/dp_rx.h
index 88bbcae..1f46d0a 100644
--- a/drivers/net/wireless/ath/ath11k/dp_rx.h
+++ b/drivers/net/wireless/ath/ath11k/dp_rx.h
@@ -92,5 +92,5 @@ int ath11k_dp_rx_mon_status_bufs_replenish(struct ath11k_base *ab, int mac_id,
 int ath11k_dp_rx_pdev_mon_detach(struct ath11k *ar);
 int ath11k_dp_rx_pdev_mon_attach(struct ath11k *ar);
 int ath11k_peer_rx_frag_setup(struct ath11k *ar, const u8 *peer_mac, int vdev_id);
-
+int ath11k_dp_purge_mon_ring(struct ath11k_base *ab);
 #endif /* ATH11K_DP_RX_H */
diff --git a/drivers/net/wireless/ath/ath11k/dp_tx.c b/drivers/net/wireless/ath/ath11k/dp_tx.c
index a6cb9c6..5958a50 100644
--- a/drivers/net/wireless/ath/ath11k/dp_tx.c
+++ b/drivers/net/wireless/ath/ath11k/dp_tx.c
@@ -1061,3 +1061,4 @@ int ath11k_dp_tx_htt_monitor_mode_ring_config(struct ath11k *ar, bool reset)
 
 	return ret;
 }
+
diff --git a/drivers/net/wireless/ath/ath11k/mac.c b/drivers/net/wireless/ath/ath11k/mac.c
index a53e62f..7d566a7 100644
--- a/drivers/net/wireless/ath/ath11k/mac.c
+++ b/drivers/net/wireless/ath/ath11k/mac.c
@@ -4023,6 +4023,11 @@ static int ath11k_mac_config_mon_status_default(struct ath11k *ar, bool enable)
 						       HAL_RXDMA_MONITOR_STATUS,
 						       DP_RX_BUFFER_SIZE, &tlv_filter);
 	}
+
+	if (enable && !ar->ab->hw_params.rxdma1_enable)
+		mod_timer(&ar->ab->mon_reap_timer, jiffies +
+			  msecs_to_jiffies(ATH11K_MON_TIMER_INTERVAL));
+
 	return ret;
 }
 
@@ -6304,3 +6309,30 @@ void ath11k_mac_destroy(struct ath11k_base *ab)
 		pdev->ar = NULL;
 	}
 }
+
+int ath11k_purge_rx_pktlog(struct ath11k *ar, bool stop_timer)
+{
+	int ret;
+
+	/* stop timer */
+	if (stop_timer)
+		del_timer_sync(&ar->ab->mon_reap_timer);
+
+	/* reap all the monitor related rings */
+	ret = ath11k_dp_purge_mon_ring(ar->ab);
+	if (ret)
+		ath11k_warn(ar->ab,
+			    "failed to purge mon_buf ring %d\n", ret);
+
+	return ret;
+}
+
+int ath11k_enable_rx_pktlog(struct ath11k *ar)
+{
+	/* start reap timer */
+	mod_timer(&ar->ab->mon_reap_timer, jiffies +
+				  msecs_to_jiffies(ATH11K_MON_TIMER_INTERVAL));
+
+	return 0;
+}
+
diff --git a/drivers/net/wireless/ath/ath11k/mac.h b/drivers/net/wireless/ath/ath11k/mac.h
index 0607479..36d8124 100644
--- a/drivers/net/wireless/ath/ath11k/mac.h
+++ b/drivers/net/wireless/ath/ath11k/mac.h
@@ -146,4 +146,6 @@ int ath11k_mac_tx_mgmt_pending_free(int buf_id, void *skb, void *ctx);
 u8 ath11k_mac_bw_to_mac80211_bw(u8 bw);
 enum ath11k_supported_bw ath11k_mac_mac80211_bw_to_ath11k_bw(enum rate_info_bw bw);
 enum hal_encrypt_type ath11k_dp_tx_get_encrypt_type(u32 cipher);
+int ath11k_purge_rx_pktlog(struct ath11k *ar, bool stop_timer);
+int ath11k_enable_rx_pktlog(struct ath11k *ar);
 #endif
diff --git a/drivers/net/wireless/ath/ath11k/wow.c b/drivers/net/wireless/ath/ath11k/wow.c
index cc50455..856a964 100644
--- a/drivers/net/wireless/ath/ath11k/wow.c
+++ b/drivers/net/wireless/ath/ath11k/wow.c
@@ -555,6 +555,12 @@ int ath11k_wow_op_suspend(struct ieee80211_hw *hw,
 
 	mutex_lock(&ar->conf_mutex);
 
+	ret = ath11k_purge_rx_pktlog(ar, true);
+	if (ret) {
+		ath11k_warn(ar->ab, "failed to disable rx pktlog: %d\n",
+			    ret);
+		goto exit;
+	}
 	ret =  ath11k_wow_cleanup(ar);
 	if (ret) {
 		ath11k_warn(ar->ab, "failed to clear wow wakeup events: %d\n",
@@ -584,6 +590,15 @@ int ath11k_wow_op_suspend(struct ieee80211_hw *hw,
 		goto cleanup;
 	}
 
+	/* when suspend_ACK is received, host needs to reap
+	 * mon_status rings again to ensure these rings are empty.
+	 */
+	ret = ath11k_purge_rx_pktlog(ar, false);
+	if (ret) {
+		ath11k_warn(ar->ab, "failed to suspend hif: %d\n", ret);
+		goto wakeup;
+	}
+
 	ret = ath11k_hif_suspend(ar->ab);
 	if (ret) {
 		ath11k_warn(ar->ab, "failed to suspend hif: %d\n", ret);
@@ -625,6 +640,12 @@ int ath11k_wow_op_resume(struct ieee80211_hw *hw)
 		goto exit;
 	}
 
+	ret = ath11k_enable_rx_pktlog(ar);
+	if (ret) {
+		ath11k_warn(ar->ab, "failed to enable rx pktlog: %d\n", ret);
+		goto exit;
+	}
+
 	ret = ath11k_wow_wakeup(ar);
 	if (ret)
 		ath11k_warn(ar->ab, "failed to wakeup from wow: %d\n", ret);
-- 
2.7.4

